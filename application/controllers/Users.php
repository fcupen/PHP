<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('page_model');
		$this->load->model('search_model');
	}
//Function: RESPONSE PAGE
function response(){

$data['pages'] = $this->page_model->data('home');

		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	

		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET articles
		$data['entries'] = $this->blog_model->getEntries('no');	
		$data['entries2'] = $this->blog_model->getEntries2('yes');
		//GET profile for the author
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();
		//GET "MY" data
		$data['profile'] = $this->blog_model->getMyData($username); 
		$data['categories'] = $this->blog_model->get_base_categories();
		if ($this->session->userdata('is_logged_in')) {
		    $data['my_entries'] = $this->blog_model->getMyEntries($username);   
		}
			$this->load->view('show_entries', $data);
	}

//Function: RANDOM ARTICLE
	function random(){

		//Random article
		$random = $this->blog_model->random_article();

	 	redirect(base_url().'users/view/'.$random->id.'/' , $data);
	}


//Function: Favorite authors
	function fav(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('fav');
//18-04-16 -- 

		$author = $this->uri->segment(3);
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$users = $this->blog_model->username_id($username_email);
		$user = $users->id;
		//**get the username with the id
		
		$fav = array(
			'user' => $user ,
			'author' => $author
			);	
		//add info
		$this->blog_model->insert('fav', $fav);



	 	redirect(base_url().'users/profileauthor/'.$author.'/' , $data);
	}
//Function: Favorite delete
	function fav_delete()
	{

	    $id_author = $this->uri->segment(3);

	    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$id_user = $user->id;
		//**get the username with the id

	    $fav= $this->blog_model->fav($id_author,$id_user);
	    $this->blog_model->deleteFav($fav->id);
	    
	    redirect(base_url().'users/profileauthor/'.$id_author.'/' , $data);
	}

//Function: Like Articles
	function like(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('like');
//18-04-16 -- 

		$article = $this->uri->segment(3);

		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$users = $this->blog_model->username_id($username_email);
		$user = $users->id;
		//**get the username with the id

		$like = array(
			'user' => $user ,
			'article' => $article
			);	
		//add info
		$this->blog_model->insert('like', $like);
	 	redirect(base_url().'users/view/'.$article.'/' , $data);
	}

//Function: Like Delete
	function like_delete()
	{
	    $id_article = $this->uri->segment(3);

	    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$id_user = $user->id;
		//**get the username with the id
	    $article= $this->blog_model->like($id_article,$id_user);
	    $this->blog_model->deleteLike($article->id);
	    
	    redirect(base_url().'users/view/'.$id_article.'/' , $data);
	}

//Function: search advance
	function search_adv(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('search_adv');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//Random article
		$data['random'] = $this->search_model->random_article();
		
		//search result
		$data['results'] = $this->search();
	 	$this->load->view('search', $data);
	}

//Function: search call model "search_model" and find the request
	function search()
	{
		
		if($this->input->post('search'))
		{
			
			//validation
			$this->form_validation->set_rules('content', 'content', 'trim|max_length[40]|xss_clean');		 
	        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');
	 		$this->form_validation->set_rules('username', 'username', 'trim|xss_clean');
				
			//three terms of the search
			$fields = array('title', 'username', 'content');

			//call DB
			$results = $this->search_model->new_search($fields);
			
			if($results !== FALSE)
			{
				
				return $results;
				
			}
			
		}
		
	}

//Function: signin / login
	function signin(){
//18-04-16 -- 
$data['pages'] = $this->page_model->data('signin');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$this->load->view('signin', $data);
	}

//Function: hide or show the articles
	function hide_show(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		$url = $this->uri->segment(3);
		//get article
		$article = $this->blog_model->getEntry($url);
		//get HIDE or SHOW variable
		$hide= $article->hide;
		
		if($hide== 'yes'){$hide='no';}else{$hide='yes';};
		
		//change DB
		$entry = array(
	        'hide'     => $hide
	        
	        );
	        //** information of the page
	    $this->blog_model->updateEntry($url, $entry, 'entries');
	    if ($this->session->userdata('is_logged_in')) {
		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**get the username with the id
		    $data['my_entries'] = $this->blog_model->getMyEntries($username); 
		    $data['entries'] = $this->blog_model->getMyEntries($username);  
		}
		//** information of the page
			redirect(base_url().'users/my_article/', $data);
			
	}
	
//Function: recover password; view
	function recover(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$this->load->view('recover_password', $data);
	}
	
//Function: recover password; send email
	function recover_password(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		$username = $this->input->post('username');
		//encrypt text string
		$url = md5($username);
		//change DB
		$entry = array(
			
			'username' => $username,
			'url' => $url
			
			);		
		$this->blog_model->insert('recover_password', $entry);

		//SEND email with the url
		$this->load->library('email');
		$config_email['mailtype']= 'html';
		$this->email->initialize($config_email);
		//i put here because i need a copy to verify , i change that later
		$this->email->from('yeah.venezuela@gmail.com');
		$this->email->to($username.','.'yeah.venezuela@gmail.com');
		$this->email->subject('New Password');
		$m1='<h1>Hello</h1><p><a href="https://astral-foundations.com/users/updt_pass/'.$url.'">Click Here</a> to create a new password </p> <p>AstralFoundations.com</p>';
		$this->email->message(''.$m1.'');
		if($this->email->send()){
		//redirect(base_url().'users/sendMailPassword/');
			redirect(base_url().'users/response/RecoverPass');
		}else{}



		
	}

//Function: update new password; view
	function updt_pass(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		$id=$this->uri->segment(3);
		//find password in the DB
		$data['pass'] = $this->blog_model->recover_password($id);

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();
		//$this->load->view('my_entries', $data);
		$this->load->view('cgpss', $data);
	}


//Function: update new password; update password
	function update_password2(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
if ($this->session->userdata('is_logged_in')) {
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;

			$pass2 = $this->input->post('password2');
			$pass = $this->input->post('password');
			//$username = $this->input->post('id');
			$id_user =$this->blog_model->username_id($username);
			//comparing passwords
			if($pass == $pass2){
				$str = md5($pass);
				 $entry = array(
		        'password'     => $str
		        );
				 $entry2 = array(
		        'url'     => ''
		        );
				//change DB
				 $this->blog_model->updateurl($username, $entry2);
			    $this->blog_model->updateUser($username, $entry);
			    redirect(base_url().'users/response/upd_passw');
			};
}else{
			$pass2 = $this->input->post('password2');
			$pass = $this->input->post('password');
			$username = $this->input->post('id');
			$id_user =$this->blog_model->username_id($username);
			//comparing passwords
			if($pass == $pass2){
				$str = md5($pass);
				 $entry = array(
		        'password'     => $str
		        );
				 $entry2 = array(
		        'url'     => ''
		        );
				//change DB
				 $this->blog_model->updateurl($username, $entry2);
			    $this->blog_model->updateUser($id_user->id, $entry);

			    redirect(base_url().'users/response/upd_passw');
		}
	}
}

//Function: send emails
	function sendMail(){
		//**
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
//FIND DATA FOR THE EMAIL
		$data = $this->blog_model->findUser($username);
		$data2 = $this->blog_model->findUser2($username);
		$data3 = $this->blog_model->email_data($data2);
		
		$this->load->library('email');
		$config_email['mailtype']= 'html';
		$this->email->initialize($config_email);
		//i put here because i need a copy to verify , i change that later
		$this->email->from('yeah.venezuela@gmail.com');
		$this->email->to($username.','.'yeah.venezuela@gmail.com');
		$this->email->subject($data3->subject);
		//choice the content
		if($data2 == 'y'){$m1='<h1>Hello</h1><p>Your Article '.$data.' '.$data3->message.'</p> <p>AstralFoundations.com</p>';};
		if($data2 == 'p'){$m1='<h1>Hello</h1><p>Your Article '.$data.' '.$data3->message.'</p> <p>please click <a href="http://astral-foundations.com">here</a> to update the article, AstralFoundations.com</p>';};
		if($data2 == 'pp'){$m1='<h1>Hello </h1><p>Your Article '.$data.' '.$data3->message.'</p> <p>AstralFoundations.com</p>';};
		if($data2 == 'n'){$m1='<h1>Hello</h1><p>Your Article '.$data.' '.$data3->message.'</p> <p>AstralFoundations.com</p>';};
		$this->email->message(''.$m1.'');
		if($this->email->send()){
		redirect(base_url().'users/my_article/');
		}else{}
	}

//Function: show my articles
	function my_article(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('my_article');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);		
		//**banner code
		$data['categories'] = $this->blog_model->get_base_categories();
		//data of the my articles
		$USER_id = $this->uri->segment(3);
		if ($this->session->userdata('is_logged_in')) {
		    //**
		     		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
			$data['profile'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username); 
		    $data['my_entries'] = $this->blog_model->getMyEntries($username); 
		    $data['entries'] = $this->blog_model->getMyEntries($username);  
		}
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

			$this->load->view('my_entries', $data);
		
	}

	//Function: edit information profile 
	function edit_info_prof(){



		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);		
		//**banner code
		$data['categories'] = $this->blog_model->get_base_categories();
		//data of the my articles
		$USER_id = $this->uri->segment(3);
		if ($this->session->userdata('is_logged_in')) {
		    //**
		     		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
			$data['information'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username); 
		    $data['my_entries'] = $this->blog_model->getMyEntries($username); 
		    $data['entries'] = $this->blog_model->getMyEntries($username);  
		}
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

			$this->load->view('signup_edit', $data);
		
	}

	function edit_info_prof2(){

$aux1 = implode(", ", $this->input->post('device'));
$aux2 = implode(", ", $this->input->post('date_avail'));
$aux3 = implode(", ", $this->input->post('expertise'));
$aux4 = implode(", ", $this->input->post('divine_practices'));

$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;

$info = array(
			'admin_priv' => $this->input->post('admin_priv'),
			'first_name'=> $this->input->post('first_name'),
			'last_name'=> $this->input->post('last_name'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city_town' => $this->input->post('city_town'),
			'state_province' => $this->input->post('state_province'),
			'zip_postal' => $this->input->post('zip_postal'),
			'country' => $this->input->post('country'),
			'phone_number' => $this->input->post('phone_number'),
			'b_month' => $this->input->post('b_month'),
			'b_day' => $this->input->post('b_day'),
			'b_year' => $this->input->post('b_year'),
			'gender' => $this->input->post('gender'),
			'q1' => $this->input->post('q1'),
			'q2' => $this->input->post('q2'),
			'q3' => $this->input->post('q3'),
			'q4' => $this->input->post('q4'),
			'q5' => $this->input->post('q5'),
			'q6' => $this->input->post('q6'),
			'q7' => $this->input->post('q7'),
			'writing_years' => $this->input->post('writing_years'),
			'has_skype' => $this->input->post('has_skype'),
			'skype_id' => $this->input->post('skype_id'),
			'has_paypal' => $this->input->post('has_paypal'),
			'based_usa' => $this->input->post('based_usa'),
			'disability' => $this->input->post('disability'),
			'ISP' => $this->input->post('ISP'),
			'pc_age' => $this->input->post('pc_age'),
			'device' => $aux1,
			'device_mac' => $this->input->post('device_mac'),
			'device_ipad' => $this->input->post('device_ipad'),
			'device_ios' => $this->input->post('device_ios'),
			'device_droid' => $this->input->post('device_droid'),
			'device_other' => $this->input->post('device_other'),
			'en_fluent' => $this->input->post('en_fluent'),
			'languages' => $this->input->post('languages'),
			'com_skills' => $this->input->post('com_skills'),
			'typing_skills' => $this->input->post('typing_skills'),
			'is_committed' => $this->input->post('is_committed'),
			'is_wee_available' => $this->input->post('is_wee_available'),
			'start_date' => $this->input->post('start_date'),
			'date_avail' => $aux2,
			'expertise' => $aux3,
			'divine_practices' => $aux4,
			'is_clairvoyant' => $this->input->post('is_clairvoyant'),
			'is_empathic' => $this->input->post('is_empathic'),
			'is_clairsentient' => $this->input->post('is_clairsentient'),
			'other_self' => $this->input->post('other_self'),
			'method_email' => $this->input->post('method_email'),
			'method_chat' => $this->input->post('method_chat'),
			'method_phone' => $this->input->post('method_phone'),
			'method_vidchat' => $this->input->post('method_vidchat'),
			'method_sms' => $this->input->post('method_sms'),
			'method_private_person' => $this->input->post('method_private_person'),
			'method_radio' => $this->input->post('method_radio'),
			'method_tv' => $this->input->post('method_tv'),
			'method_public_venue' => $this->input->post('method_public_venue'),
			'skills' => $this->input->post('skills'),
			'exp_years' => $this->input->post('exp_years'),
			'companies' => $this->input->post('companies'),
			'articles' => $this->input->post('articles'),
			'blogs' => $this->input->post('blogs'),
			'bio' => $this->input->post('bio')
			
			);  


		$this->blog_model->updateUser($username, $info);

			redirect(base_url());
		
	}

//Function: show articles with the categories
	function category(){
		
//**DATA CONTENT
$data['pages'] = $this->page_model->data('category');
//**DATA CONTENT

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	

		//get the id
		$category_id = $this->uri->segment(3);
		//get articles of the category
		$data['entries'] = $this->blog_model->getCategory($category_id);
		
		//data of the page
		$data['category'] = $this->blog_model->getCat($category_id);
		$data['categories'] = $this->blog_model->get_base_categories();
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();

		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

			$this->load->view('category', $data);
		
	}
	

//Function: show all categories
	function categories(){
		
//**DATA CONTENT
$data['pages'] = $this->page_model->data('categories');
//**DATA CONTENT

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	

		//get the id
		$category_id = $this->uri->segment(3);
		//get articles of the category
		$data['entries'] = $this->blog_model->getCategory($category_id);
		
		//data of the page
		$data['categories'] = $this->blog_model->get_base_categories();
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

			$this->load->view('categories', $data);
		
	}
	
//Function: show articles with the categories
	function categories2(){
		
//**DATA CONTENT
$data['pages'] = $this->page_model->data('category');
//**DATA CONTENT

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	

		//get the id
		$category_id = $this->uri->segment(3);
		//get articles of the category
		$data['entries'] = $this->blog_model->getCategory($category_id);
		    		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//data of the page
		$data['categories'] = $this->blog_model->get_base_categories();
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

			$this->load->view('category', $data);
		
	}
	
//Function: add new article; choice category or add new category
	function entry(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('entry');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
		//get all categories
		$data['profile'] = $this->blog_model->getMyData($username);
		$data['categories'] = $this->blog_model->get_base_categories();
		$data['cat'] = $this->blog_model->categ2();
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$this->load->view('new_entry_c', $data);
	}

//Function: new article; view
	function entry2(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('entry2');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//get data 
		$data['profile'] = $this->blog_model->getMyData($username);
		$data['categories'] = $this->blog_model->get_base_categories();
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$this->load->view('new_entry', $data);
	}

//Function: update profile; view
	function profileupdate(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profileupdate');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		if ($this->session->userdata('is_logged_in')) {

		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//get "my" data
		    $data['profile'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username);     
		$this->load->view('profileInfo', $data);
		}else{$this->load->view('show_entries');};
		
	}

//Function: update profile; imagen, name and username
	function profupdate(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profupdate');
//18-04-16 -- 

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//create new name for the imagen
$aux = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux2=20;
$letter = "";
for($i=0;$i<$aux2;$i++)
{
    $letter .= substr($aux,rand(0,strlen($aux)),1); 
}
		//upload img
        $config['upload_path'] = './public/img/';
        $config['file_name'] = $letter;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=array('error' => $this->upload->display_errors());
                //$this->load->view('subir_view', $error);
        }else{
            $datos["img"]=$this->upload->data();
            $datosa=$this->upload->data("orig_name");
           // $this->load->view('subir_view', $datos);
        }
        //get data for the form
        $inp2 = $this->input->post('name');
        $inp3 = $this->input->post('username');
		if ($this->session->userdata('is_logged_in')) {
			//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
//change DB
if(!$this->upload->do_upload()){
             $entry = array(
		        
		        'name' => $inp2,
		        'username' => $inp3
		        );     
        }else{
	     $entry = array(
		        'imagen'     => $datosa,
		        'name' => $inp2,
		        'username' => $inp3
		        );};

				
			    $this->blog_model->updateUser($username, $entry);

			    	
				redirect(base_url().'users/response/upd_profile/');	
		}else{
			$this->load->view('show_entries');
		};
		
	}

//Function: update profile;VERIF EMAIL
	function verif(){
$key = $this->uri->segment(3);
			//**get the username with the id
		$user = $this->blog_model->verif($key);
		$username = $user->id;
		//**
//change DB 
             $entry = array(
		        'verif' => 's'
		        );     
			    $this->blog_model->updateUser($username, $entry);	
				redirect(base_url());	
	}

//Function: FEATURED AUTORS
	function featured_authors(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('featured_authors');
//18-04-16 -- 



		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET profile for the author
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();


		//GET "MY" data
		$data['profile'] = $this->blog_model->getMyData($username); 
		$data['categories'] = $this->blog_model->get_base_categories();
		if ($this->session->userdata('is_logged_in')) {
		    $data['my_entries'] = $this->blog_model->getMyEntries($username);   
		}
		
			$this->load->view('featured-authors.php', $data);
		
	}

//
function favorite_articles(){


//18-04-16 -- 
$data['pages'] = $this->page_model->data('favorite_articles');
//18-04-16 -- 


		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET profile for the author
		$data['fav'] = $this->blog_model->favArticle($username);	

		
			$this->load->view('favorite-articles.php', $data);
		
	}

//
function favorite_authors(){


//18-04-16 -- 
$data['pages'] = $this->page_model->data('favorite_authors');
//18-04-16 -- 


		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET profile for the author
		$data['fav'] = $this->blog_model->favAuthor($username);	
		
			$this->load->view('favorite-authors.php', $data);
		
	}

//Function: Reports of invoice, payments etc.
	function reports(){


//18-04-16 -- 
$data['pages'] = $this->page_model->data('reports');
//18-04-16 -- 


		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET reports for the author
		$data['reports'] = $this->blog_model->getReports($username);	

		
			$this->load->view('reports.php', $data);
		
	}

	function invoice(){


//18-04-16 -- 
$data['pages'] = $this->page_model->data('invoice');
//18-04-16 -- 


		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//GET "MY" data
		$data['profile'] = $this->blog_model->getMyData($username); 
		$data['categories'] = $this->blog_model->get_base_categories();
		if ($this->session->userdata('is_logged_in')) {
		    $data['my_entries'] = $this->blog_model->getMyEntries($username);   
		}
		
			$this->load->view('invoice.php', $data);
		
	}

//
	function suggest_cat(){


//18-04-16 -- 
$data['pages'] = $this->page_model->data('suggest_cat');
//18-04-16 -- 


		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		     		
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$data['cat'] = $this->blog_model->categ2();
		$data['categories'] = $this->blog_model->get_base_categories();
		//GET "MY" data
		$data['profile'] = $this->blog_model->getMyData($username); 
		$data['categories'] = $this->blog_model->get_base_categories();
		if ($this->session->userdata('is_logged_in')) {
		    $data['my_entries'] = $this->blog_model->getMyEntries($username);   
		}
		
			$this->load->view('suggest_cat.php', $data);
		
	}

//Function: profile of the author
	function profileauthor(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profileauthor');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//get id for the author
		$USER_id = $this->uri->segment(3);

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username_id = $user->id;
		//**

		//FAVORITE AUTHOR
		$data['fav'] = $this->blog_model->getFav($username_id,$USER_id);


		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		if ($this->session->userdata('is_logged_in')) {
			//info for the page
		    $data['profile'] = $this->blog_model->getinfo_user2($USER_id);  
		    $data['entries'] = $this->blog_model->getMyEntries($USER_id); 
		    $data['profile2'] = $this->blog_model->getinfo_user2b($USER_id); 
		$this->load->view('profileauthor', $data);
		}else{redirect(base_url().'users/response/need_log');	};
		
	}

//Function: "my" profile
	function profile(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profile');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		if ($this->session->userdata('is_logged_in')) {

		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//data for the page
		    $data['profile'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username);     
		$this->load->view('profile', $data);
		}else{$this->load->view('show_entries');};
		
	}

//Function: update own profile; view
	function profile2(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profile2');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		if ($this->session->userdata('is_logged_in')) {
		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//get data for the form
		    $data['profile'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username);     
		$this->load->view('changeprof', $data);
		}else{$this->load->view('show_entries');};
		
	}
//Function: update "my" profile; bio and area
	function profile3(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('profile3');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		if ($this->session->userdata('is_logged_in')) {
			//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//get data for the form
			$inp1 = $this->input->post('bio');
			$inp2 = $this->input->post('area');

				 $entry = array(
		        'bio'     => $inp1,
		        'area'     => $inp2,
		        'user' => $username
		        );
				 //change DB
			    $this->blog_model->updateUser2($username, $entry);

			    redirect(base_url().'users/profile/upd_profile/');
			

		}else{
			$this->load->view('show_entries');
		};
		
	}

//Function: change the password; verify past password
	function pass(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data2['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data2['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
		$data2['footer'] = $this->blog_model->footer();
		$data2['footer_social'] = $this->blog_model->footer_social();
		
		//CENTRAL MENU
		$data2['central_menu'] = $this->blog_model->central_menu();
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		$data2['profile'] = $this->blog_model->getMyData($username); 


		if ($this->session->userdata('is_logged_in')) {

		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		$this->load->view('verifpass', $data);
		}else{$this->load->view('show_entries',$data2);};

		
	}

//Function: change the password; view
	function pass2(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		if ($this->session->userdata('is_logged_in')) {
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//GET the old password and verify
			$pass2 = $this->blog_model->getMyData($username);    
			$pass = $this->input->post('password');
			$str = md5($pass);
			if($str == $pass2->password){
		    //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		    $data['profile'] = $this->blog_model->getMyData($username); 
		    $data['profile2'] = $this->blog_model->getinfo_user($username);     
		$this->load->view('changepass', $data);
		}else{
			//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		    $data['profile'] = $this->blog_model->getMyData($username);    
		$this->load->view('verifpass', array('error'=>TRUE));};

	}else{$this->load->view('show_entries');};
		
	}

//Function: change the password
function pass3(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		if ($this->session->userdata('is_logged_in')) {
			//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//compare the two password
			$pass2 = $this->input->post('password2');
			$pass = $this->input->post('password');
			
			if($pass == $pass2){
				$str = md5($pass);
				 $entry = array(
		        'password'     => $str
		        );
			    $this->blog_model->updateUser($username, $entry);

			    redirect(base_url().'users/response/upd_passw/');
			}else{
				//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
			    $data['profile'] = $this->blog_model->getMyData($username);    
				$this->load->view('changepass', array('error'=>TRUE));
			};

		}else{
			$this->load->view('show_entries');
		};
		
	}
	
//Function: view the article
	function view(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('view');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$dat2a['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$dat2a['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$dat2a['footer'] = $this->blog_model->footer();
		$dat2a['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$dat2a['central_menu'] = $this->blog_model->central_menu();

		//SIDEMENU
		$dat2a['side_menu'] = $this->blog_model->side_menu();
		
		//id of the article
		$entry_id = $this->uri->segment(3);
		if ($this->session->userdata('is_logged_in')) {
			//data of the article
		$username = $this->session->userdata('username');
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username_id = $user->id;
		//**
		$dat2a['profile'] = $this->blog_model->getMyData($username); };
		$dat2a['comments'] = $this->blog_model->getComments($entry_id);
		$dat2a['entr'] = $this->blog_model->getEntry($entry_id);
		$dat2a['like'] = $this->blog_model->getLike($entry_id,$username_id);

		//upload visits
		$auux=$this->blog_model->getEntry($entry_id)->visits+1;
		$entry = array(
	        'visits'     => $auux);

	        //change DB
	    $this->blog_model->updateEntry($entry_id, $entry, 'entries');

		//--
		$this->load->view('view_entry', $dat2a);
	}

//Function: add new article
	function insert_entry(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('insert_entry');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//new name for the cover
$aux = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux2=20;
$letter = "";
for($i=0;$i<$aux2;$i++)
{
    $letter .= substr($aux,rand(0,strlen($aux)),1); 
}
		//upload cover
        $config['upload_path'] = './public/articles/';
        $config['file_name'] = $letter;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=$this->upload->display_errors();
        }else{
            $datos["img"]=$this->upload->data();
            $datosa=$this->upload->data("orig_name");
        }

        //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**

		$cont = $this->input->post('content');
		$str = $cont;
$str = strtolower($str);
$data = $this->blog_model->getApproval($str);
//approved variable
if($data >= '1'){$aaux= 'p';}else{$aaux= 'pp';};
//---
		$entry = array(
			'author' => $username,
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'date' => date('Y-m-d H:i:s'),
			'tags' => $this->input->post('tags'),
			'category' => $this->input->post('category'),
			'approved' => $aaux,
			'cover'=> $datosa
			);		
		//change DB
		$this->blog_model->insert('entries', $entry);

		redirect(base_url().'users/sendmail/');
	}

//Function: submit invoice
	function submit_invoice(){
		
		//new name for the cover
$aux = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux2=20;
$letter = "";
for($i=0;$i<$aux2;$i++)
{
    $letter .= substr($aux,rand(0,strlen($aux)),1); 
}
		//upload cover
        $config['upload_path'] = './public/invoice/';
        $config['file_name'] = $letter;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=$this->upload->display_errors();
        }else{
        	$datos["img"]=$this->upload->data();
            $datosa=$this->upload->data("orig_name");
        }

        //**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		
//---
		$entry = array(
			'author' => $username,
			'invoice'=> $datosa
			);		
		//change DB
		$this->blog_model->insert('invoice', $entry);

		redirect(base_url().'users/my_article/sub_invoice/');
	}

//Function: add new suggest category
	function insert_cat(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	



		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//get category
		$cat = $this->input->post('inpt');
		$cat_ID = $this->input->post('id')+1;
//---
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
if($this->session->userdata('is_logged_in')){	$category = array(
			
			'user' => $username ,
			'name_cat' => $this->input->post('inpt')/*,
			'category_id' => $this->input->post('category')*/
			
			);	}else{$category = array(
			
			'user' => $this->input->post('user') ,
			'name_cat' => $this->input->post('inpt')/*,
			'category_id' => $this->input->post('category')*/
			
			);
		
	};	
		//add info
		$this->blog_model->insert('category', $category);
if($this->session->userdata('is_logged_in')){	
		redirect(base_url().'users/my_article/sub_sugg_cat/');
	}else{
		redirect(base_url().'users/response/sub_sugg_cat/');};
	}

//Function: add comments
	function comment(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**
		//get ID
		$id = $this->input->post('id_article');
		$comment = array(
			'id_article' => $id,
			'author' => $username_email,
			'comment' => $this->input->post('comment'),
			'date' => date('Y-m-d H:i:s')
			);
//add comment
		$this->blog_model->insert('comments', $comment);

		redirect(base_url().'users/view/'.$id);
	}


//Function: update article; view
	function edit()
	{
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//get categories
		$data['categories'] = $this->blog_model->get_base_categories();
		//id for the article
	    $id_entry = $this->uri->segment(3);
	    //data for the article
        $data['entry_data'] = $this->blog_model->getEntryData($id_entry);	  
        
	    $this->load->view('upd_entry', $data);
	}
	
//Function: update article; update
	function update_entry()
	{
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//new name for the cover
$aux = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux2=20;
$letter = "";
for($i=0;$i<$aux2;$i++)
{
    $letter .= substr($aux,rand(0,strlen($aux)),1); 
}
		//upload cover
        $config['upload_path'] = './public/articles/';
        $config['file_name'] = $letter;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=array('error' => $this->upload->display_errors());
               
        }else{
            $datos["img"]=$this->upload->data();
            $datosa=$this->upload->data("orig_name");
           
        }


		//----
		$content = $this->input->post('content');
$str = $content;
$str = strtolower($str);
$data = $this->blog_model->getApproval($str);
//variable approval
if($data >= '1'){$aaux= 'p';}else{$aaux= 'pp';};
	    $id = $this->input->post('id');
if(!$this->upload->do_upload()){
            $entry = array(
	        'title'     => $this->input->post('title'),
	        'content'   => $this->input->post('content'),
	        'tags'      => $this->input->post('tags'),
	        'category' => $this->input->post('category'),
	        'approved' => $aaux
	        );     
        }else{
	    $entry = array(
	        'title'     => $this->input->post('title'),
	        'content'   => $this->input->post('content'),
	        'tags'      => $this->input->post('tags'),
	        'category' => $this->input->post('category'),
	        'approved' => $aaux,
	        'cover'=> $datosa
	        );};
	        //change DB
	    $this->blog_model->updateEntry($id, $entry, 'entries');
	    
	    redirect(base_url().'users/sendmail/');
	}
	
//Function: signup new user; view
	function signup(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('signup');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();
		
	
		$this->load->view('signup',$data);
	
	}

//Function: signup new user; view
	function signup_2(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('signup');
//18-04-16 -- 

		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//SIDEMENU
		$data['side_menu'] = $this->blog_model->side_menu();
		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();
$this->load->view('menu',$data);
		$this->load->view('signup_2',$data);
		$this->load->view('footer',$data);
	}

//Function: signup new user
	function register(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//new name for the imagen
$aux = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux2=20;
$letter = "";
for($i=0;$i<$aux2;$i++)
{
    $letter .= substr($aux,rand(0,strlen($aux)),1); 
}

		//upload imagen
        $config['upload_path'] = './public/img/';
        $config['file_name'] = $letter;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=array('error' => $this->upload->display_errors());
                //$this->load->view('subir_view', $error);
        }else{
            $datos["img"]=$this->upload->data();
            $datosa=$this->upload->data("orig_name");
           // $this->load->view('subir_view', $datos);
        }

		
		$username = $this->input->post('username');
		$ayx =$this->input->post('admin_priv');
		if($ayx != ''){$admin_priv = $this->input->post('admin_priv');}else{$admin_priv = 'male.png';};
		
		$imagen = $this->input->post('file_name');
		//CAPCHAT DATA

		$captcha = $this->input->post('g-recaptcha-response');
		$recaptcha_secret = "6Ldh8BYTAAAAAPXkuUitK21dSpo_hbHKRjk93723";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$captcha);
        $status = json_decode($response, true);
		$imagen = $this->file->UploadImage('./public/img/','ERROR Please fill the Capchat');

//code for verification email
	$aux3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$aux4=20;
$letter = "";
for($i=0;$i<$aux4;$i++)
{
    $letter2 .= substr($aux3,rand(0,strlen($aux3)),1); 
}

$aux1 = implode(", ", $this->input->post('device'));
$aux2 = implode(", ", $this->input->post('date_avail'));
$aux3 = implode(", ", $this->input->post('expertise'));
$aux4 = implode(", ", $this->input->post('divine_practices'));


if($status["success"] === true)
        {

        	//upload imagen
if(!$this->upload->do_upload()){
        $user = array(
        	'invited' => 'n',
			'name' => $name,
			'username' => $username,
			'password' => md5($this->input->post('password')),
			'admin_priv' => $this->input->post('admin_priv'),
			'verif'=>$letter2,
			'first_name'=> $this->input->post('first_name'),
			'last_name'=> $this->input->post('last_name'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city_town' => $this->input->post('city_town'),
			'state_province' => $this->input->post('state_province'),
			'zip_postal' => $this->input->post('zip_postal'),
			'country' => $this->input->post('country'),
			'phone_number' => $this->input->post('phone_number'),
			'b_month' => $this->input->post('b_month'),
			'b_day' => $this->input->post('b_day'),
			'b_year' => $this->input->post('b_year'),
			'gender' => $this->input->post('gender'),
			'q1' => $this->input->post('q1'),
			'q2' => $this->input->post('q2'),
			'q3' => $this->input->post('q3'),
			'q4' => $this->input->post('q4'),
			'q5' => $this->input->post('q5'),
			'q6' => $this->input->post('q6'),
			'q7' => $this->input->post('q7'),
			'writing_years' => $this->input->post('writing_years'),
			'has_skype' => $this->input->post('has_skype'),
			'has_paypal' => $this->input->post('has_paypal'),
			'based_usa' => $this->input->post('based_usa'),
			'disability' => $this->input->post('disability'),
			'ISP' => $this->input->post('ISP'),
			'pc_age' => $this->input->post('pc_age'),
			'device' => $aux1,
			'device_mac' => $this->input->post('device_mac'),
			'device_ipad' => $this->input->post('device_ipad'),
			'device_ios' => $this->input->post('device_ios'),
			'device_droid' => $this->input->post('device_droid'),
			'device_other' => $this->input->post('device_other'),
			'en_fluent' => $this->input->post('en_fluent'),
			'languages' => $this->input->post('languages'),
			'com_skills' => $this->input->post('com_skills'),
			'typing_skills' => $this->input->post('typing_skills'),
			'is_committed' => $this->input->post('is_committed'),
			'is_wee_available' => $this->input->post('is_wee_available'),
			'start_date' => $this->input->post('start_date'),
			'date_avail' => $aux2,
			'expertise' => $aux3,
			'divine_practices' => $aux4,
			'is_clairvoyant' => $this->input->post('is_clairvoyant'),
			'is_empathic' => $this->input->post('is_empathic'),
			'is_clairsentient' => $this->input->post('is_clairsentient'),
			'other_self' => $this->input->post('other_self'),
			'method_email' => $this->input->post('method_email'),
			'method_chat' => $this->input->post('method_chat'),
			'method_phone' => $this->input->post('method_phone'),
			'method_vidchat' => $this->input->post('method_vidchat'),
			'method_sms' => $this->input->post('method_sms'),
			'method_private_person' => $this->input->post('method_private_person'),
			'method_radio' => $this->input->post('method_radio'),
			'method_tv' => $this->input->post('method_tv'),
			'method_public_venue' => $this->input->post('method_public_venue'),
			'skills' => $this->input->post('skills'),
			'exp_years' => $this->input->post('exp_years'),
			'companies' => $this->input->post('companies'),
			'articles' => $this->input->post('articles'),
			'blogs' => $this->input->post('blogs'),
			'bio' => $this->input->post('bio'),
			'agree_terms' => $this->input->post('agree_terms'),
			'accept_contractual' => $this->input->post('accept_contractual'),
			'accept_responsibility' => $this->input->post('accept_responsibility')
			
			);  
        }else{
	    $user = array(
			'invited' => 'n',
			
			'imagen' => $imagen,
			'username' => $username,
			'password' => md5($this->input->post('password')),
			'admin_priv' => $this->input->post('admin_priv'),
			'verif'=>$letter2,
			'first_name'=> $this->input->post('first_name'),
			'last_name'=> $this->input->post('last_name'),
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city_town' => $this->input->post('city_town'),
			'state_province' => $this->input->post('state_province'),
			'zip_postal' => $this->input->post('zip_postal'),
			'country' => $this->input->post('country'),
			'phone_number' => $this->input->post('phone_number'),
			'b_month' => $this->input->post('b_month'),
			'b_day' => $this->input->post('b_day'),
			'b_year' => $this->input->post('b_year'),
			'gender' => $this->input->post('gender'),
			'q1' => $this->input->post('q1'),
			'q2' => $this->input->post('q2'),
			'q3' => $this->input->post('q3'),
			'q4' => $this->input->post('q4'),
			'q5' => $this->input->post('q5'),
			'q6' => $this->input->post('q6'),
			'q7' => $this->input->post('q7'),
			'writing_years' => $this->input->post('writing_years'),
			'has_skype' => $this->input->post('has_skype'),
			'has_paypal' => $this->input->post('has_paypal'),
			'based_usa' => $this->input->post('based_usa'),
			'disability' => $this->input->post('disability'),
			'ISP' => $this->input->post('ISP'),
			'pc_age' => $this->input->post('pc_age'),
			'device' => $aux1,
			'device_mac' => $this->input->post('device_mac'),
			'device_ipad' => $this->input->post('device_ipad'),
			'device_ios' => $this->input->post('device_ios'),
			'device_droid' => $this->input->post('device_droid'),
			'device_other' => $this->input->post('device_other'),
			'en_fluent' => $this->input->post('en_fluent'),
			'languages' => $this->input->post('languages'),
			'com_skills' => $this->input->post('com_skills'),
			'typing_skills' => $this->input->post('typing_skills'),
			'is_committed' => $this->input->post('is_committed'),
			'is_wee_available' => $this->input->post('is_wee_available'),
			'start_date' => $this->input->post('start_date'),
			'date_avail' => $aux2,
			'expertise' => $aux3,
			'divine_practices' => $aux4,
			'is_clairvoyant' => $this->input->post('is_clairvoyant'),
			'is_empathic' => $this->input->post('is_empathic'),
			'is_clairsentient' => $this->input->post('is_clairsentient'),
			'other_self' => $this->input->post('other_self'),
			'method_email' => $this->input->post('method_email'),
			'method_chat' => $this->input->post('method_chat'),
			'method_phone' => $this->input->post('method_phone'),
			'method_vidchat' => $this->input->post('method_vidchat'),
			'method_sms' => $this->input->post('method_sms'),
			'method_private_person' => $this->input->post('method_private_person'),
			'method_radio' => $this->input->post('method_radio'),
			'method_tv' => $this->input->post('method_tv'),
			'method_public_venue' => $this->input->post('method_public_venue'),
			'skills' => $this->input->post('skills'),
			'exp_years' => $this->input->post('exp_years'),
			'companies' => $this->input->post('companies'),
			'articles' => $this->input->post('articles'),
			'blogs' => $this->input->post('blogs'),
			'bio' => $this->input->post('bio'),
			'agree_terms' => $this->input->post('agree_terms'),
			'accept_contractual' => $this->input->post('accept_contractual'),
			'accept_responsibility' => $this->input->post('accept_responsibility')
			);};

	    //**SEND EMAIL"NEW USER"
	    $data_email = $this->blog_model->email_data('new_user');
		$this->load->library('email');
		$config_email['mailtype']= 'html';
		$this->email->initialize($config_email);
		$this->email->from('yeah.venezuela@gmail.com');
		$this->email->to($username.','.'yeah.venezuela@gmail.com');
		$this->email->subject($data_email->subject);
		$m1=$data_email->message;
		$this->email->message(''.$m1.'<a href="https://www.astral-foundations.com/users/verif/'.$letter2.'" > HERE</a>');
		if($this->email->send()){
		
		}else{}

		if($this->blog_model->insert('users', $user)){
			$session = array(
				'name' => $name,
				'username' => $username,
				'is_logged_in' => TRUE,				
				);
			$this->session->set_userdata($session);
		$data['username'] = $username;
			redirect(base_url().'users/signup_2');
		}
	}else
		{
			$this->load->view('signup', array('error'=>TRUE));
		}

	}

function test(){

$filename="application/views/test.php";
$content='<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HVAC Rater Helper - Edit Designs</title>

</head>

<body>
 
fasdf
 
</body>

</html>';

file_put_contents($filename, $content) ;



}


//Function: signup new user
	function register2(){
		//**banner code
		$banner1 = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);	
		//**banner code
		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	
				
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();
$aux1 = implode(", ", $this->input->post('device'));
$aux2 = implode(", ", $this->input->post('date_avail'));
$aux3 = implode(", ", $this->input->post('expertise'));
$aux4 = implode(", ", $this->input->post('divine_practices'));



        $info = array(
        	
			'q1' => $this->input->post('q1'),
			'q2' => $this->input->post('q2'),
			'q3' => $this->input->post('q3'),
			'q4' => $this->input->post('q4'),
			'q5' => $this->input->post('q5'),
			'q6' => $this->input->post('q6'),
			'q7' => $this->input->post('q7'),
			'writing_years' => $this->input->post('writing_years'),
			'has_skype' => $this->input->post('has_skype'),
			'skype_id' => $this->input->post('skype_id'),
			'has_paypal' => $this->input->post('has_paypal'),
			'based_usa' => $this->input->post('based_usa'),
			'disability' => $this->input->post('disability'),
			'ISP' => $this->input->post('ISP'),
			'pc_age' => $this->input->post('pc_age'),
			'device' => $aux1,
			'device_mac' => $this->input->post('device_mac'),
			'device_ipad' => $this->input->post('device_ipad'),
			'device_ios' => $this->input->post('device_ios'),
			'device_droid' => $this->input->post('device_droid'),
			'device_other' => $this->input->post('device_other'),
			'en_fluent' => $this->input->post('en_fluent'),
			'languages' => $this->input->post('languages'),
			'com_skills' => $this->input->post('com_skills'),
			'typing_skills' => $this->input->post('typing_skills'),
			'is_committed' => $this->input->post('is_committed'),
			'is_wee_available' => $this->input->post('is_wee_available'),
			'start_date' => $this->input->post('start_date'),
			'date_avail' => $aux2,
			'expertise' => $aux3,
			'divine_practices' => $aux4,
			'is_clairvoyant' => $this->input->post('is_clairvoyant'),
			'is_empathic' => $this->input->post('is_empathic'),
			'is_clairsentient' => $this->input->post('is_clairsentient'),
			'other_self' => $this->input->post('other_self'),
			'method_email' => $this->input->post('method_email'),
			'method_chat' => $this->input->post('method_chat'),
			'method_phone' => $this->input->post('method_phone'),
			'method_vidchat' => $this->input->post('method_vidchat'),
			'method_sms' => $this->input->post('method_sms'),
			'method_private_person' => $this->input->post('method_private_person'),
			'method_radio' => $this->input->post('method_radio'),
			'method_tv' => $this->input->post('method_tv'),
			'method_public_venue' => $this->input->post('method_public_venue'),
			'skills' => $this->input->post('skills'),
			'exp_years' => $this->input->post('exp_years'),
			'companies' => $this->input->post('companies'),
			'articles' => $this->input->post('articles'),
			'blogs' => $this->input->post('blogs'),
			'bio' => $this->input->post('bio')
			
			);  
		
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username_id = $user->id;
		//**

		
		if($this->blog_model->updateEntry($username_id, $info, 'users')){
			redirect(base_url());
		
	}else
		{
			$this->load->view('signup_2', array('error'=>TRUE));
		}

	}

//Function: verify information
	function validate(){		
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		//CAPTCHA INFO
		$captcha = $this->input->post('g-recaptcha-response');
		$recaptcha_secret = "6Ldh8BYTAAAAAPXkuUitK21dSpo_hbHKRjk93723";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$captcha);
        $status = json_decode($response, true);


if($status["success"] === true)
        {
            
        

//VALIDATE INFO
			if($user = $this->blog_model->validate_credentials($username, $password)){
				$session = array(
					'name' => $user->name,
					'username' => $username,
					'is_logged_in' => TRUE,				
					);
				$this->session->set_userdata($session);

				redirect(base_url());
			}
			else{
				$this->load->view('signin', array('error'=>TRUE));
			}
		}
        else
        {
            $this->load->view('signin', array('error'=>TRUE));
        }
	}
function delete()
	{
	    $id_entry = $this->uri->segment(3);
	    
	    $this->blog_model->deleteEntry($id_entry);
	    
	    redirect(base_url().'users/my_article');
	}
//Function: logout
	function logout(){
		if($this->session->userdata('is_logged_in'))
			$this->session->sess_destroy();		
		redirect(base_url());			
	}

}