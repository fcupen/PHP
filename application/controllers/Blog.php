<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('blog_model');	
		$this->load->model('page_model');	
	}

	function index(){

//18-04-16 -- 
$data['pages'] = $this->page_model->data('home');
//18-04-16 -- 

		//**banner code
		$banner1 = 'home*';
		$data['banner'] = $this->blog_model->getBanner($banner1);
		$banner2 = 'all*';
		$data['banner2'] = $this->blog_model->getBannerAll($banner2);
		//**banner code	
		//**get the username with the id
		$username_email = $this->session->userdata('username');
		$user = $this->blog_model->username_id($username_email);
		$username = $user->id;
		//**

		//FOOTER
		$data['footer'] = $this->blog_model->footer();
		$data['footer_social'] = $this->blog_model->footer_social();	

		
		//CENTRAL MENU
		$data['central_menu'] = $this->blog_model->central_menu();

		//GET articles
		$data['entries'] = $this->blog_model->getEntries('no');	
		$data['entries2'] = $this->blog_model->getEntries2('yes');
		//GET profile for the author
		$data['featured'] = $this->blog_model->featuredAuthor();	
		$data['featured2'] = $this->blog_model->featuredAuthorRow();
		$data['featured3'] = $this->blog_model->featuredAuthorRow2();
		//GET "MY" data
		$data['profile'] = $this->blog_model->getMyData($username); 
		$data['categories'] = $this->blog_model->get_base_categories();
		if ($this->session->userdata('is_logged_in')) {
		    
		    $data['my_entries'] = $this->blog_model->getMyEntries($username);   

		}

			$this->load->view('show_entries', $data);
	
	}

	
}