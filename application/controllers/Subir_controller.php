<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subir_controller extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('blog_model');
	}

	 public function index(){	
        $this->load->view('subir_view');
   }
   
   public function subir(){
$caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; 
$numerodeletras=20;
$cadena = "";
for($i=0;$i<$numerodeletras;$i++)
{
    $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); 
}
echo $cadena;
        $config['upload_path'] = './public/';
        $config['file_name'] = $cadena;
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
                $error=array('error' => $this->upload->display_errors());
                $this->load->view('subir_view', $error);
        }else{
            $datos["img"]=$this->upload->data();

            // Podemos acceder a todas las propiedades del fichero subido 
            // $datos["img"]["file_name"]);
            $this->load->view('subir_view', $datos);
        }
    }	

}

