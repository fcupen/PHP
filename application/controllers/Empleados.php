<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Empleados extends CI_Controller
	{
	
		function __construct()
		{
		
			parent::__construct();
			
			if(!$this->session->userdata('admin_is_logged'))
			{
				redirect('/vadmin/login');
				exit;
			}
			 $this->load->model("Empleados_model");
			
		}
	

	function index()
	{
		$this->load->view('frontend/empleados');
	}

	function guardar(){
		//El metodo is_ajax_request() de la libreria input permite verificar
		//si se esta accediendo mediante el metodo AJAX 
		if ($this->input->is_ajax_request()) {
			$nombres = $this->input->post("nombres");
			$apellidos = $this->input->post("apellidos");
			$dni = $this->input->post("dni");
			$telefono = $this->input->post("telefono");
			$email = $this->input->post("email");

			$datos = array(
				"nombres_empleado" => $nombres,
				"apellidos_empleado" => $apellidos,
				"dni_empleado" => $dni,
				"telefono_empleado" => $telefono,
				"email_empleado" => $email
				);
			if($this->Empleados_model->guardar($datos)==true)
				echo "Registro Guardado";
			else
				echo "No se pudo guardar los datos";
		}
		else
		{
			show_404();
		}


	}


	function mostrar(){
		//El metodo is_ajax_request() de la libreria input permite verificar
		//si se esta accediendo mediante el metodo AJAX 
		if ($this->input->is_ajax_request()) {
			$buscar = $this->input->post("buscar");
			$datos = $this->Empleados_model->mostrar($buscar);
			echo json_encode($datos);
		}
		else
		{
			show_404();
		}


	}


}

