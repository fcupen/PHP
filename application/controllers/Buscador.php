<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Buscador extends CI_Controller 
{
	
	public function __construct() 
	{
		
		parent::__construct();
		$this->load->database('default');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->model('buscador_model');
		
	}
	
	public function index()
	{

		$data = array('titulo' => 'Buscador con múltiples criterios', 
					  'resultados' => $this->busqueda());
			$this->load->view('vadmin/header');		  
		$this->load->view('buscador_view',$data);
		$this->load->view('vadmin/footer');
	}
	
	public function busqueda()
	{
		
		if($this->input->post('buscar'))
		{
			
	
			$this->form_validation->set_rules('content', 'content', 'trim|max_length[40]|xss_clean');		 
	        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');
	 		$this->form_validation->set_rules('author', 'author', 'trim|xss_clean');
				
			$campos = array('title', 'author', 'content');

			$resultados = $this->buscador_model->nueva_busqueda($campos);
			
			if($resultados !== FALSE)
			{
				
				return $resultados;
				
			}
			
		}
		
	}
 
	
}