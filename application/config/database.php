<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);

$active_group = 'production';
$query_builder = TRUE;

if(!empty($_SERVER['ENVIRONMENT'])){
    $active_group = $_SERVER['ENVIRONMENT'];
}
//***--


$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'astfou_db';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

//--- Production Credentials (CURRENTLY DOESN'T EXIST)
$db['production']['hostname'] = 'localhost';
$db['production']['username'] = 'root';
$db['production']['password'] = '';
$db['production']['database'] = 'astfou_db';
$db['production']['dbdriver'] = 'mysqli';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = FALSE;
$db['production']['db_debug'] = TRUE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;

//--- Staging Credentials (dev.psychic-contact.com)
$db['development']['hostname'] = 'localhost';
$db['development']['username'] = 'astfou_user';
$db['development']['password'] = 'CDH^yQ@!e9%O';
$db['development']['database'] = 'astfou_db';
$db['development']['dbdriver'] = 'mysqli';
$db['development']['dbprefix'] = '';
$db['development']['pconnect'] = FALSE;
$db['development']['db_debug'] = TRUE;
$db['development']['cache_on'] = FALSE;
$db['development']['cachedir'] = '';
$db['development']['char_set'] = 'utf8';
$db['development']['dbcollat'] = 'utf8_general_ci';
$db['development']['swap_pre'] = '';
$db['development']['autoinit'] = TRUE;
$db['development']['stricton'] = FALSE;

//--- Woodjh Configuration
$db['woodjh']['hostname'] = 'owws.com';
$db['woodjh']['username'] = 'astfou_user';
$db['woodjh']['password'] = 'CDH^yQ@!e9%O';
$db['woodjh']['database'] = 'astfou_db';
$db['woodjh']['dbdriver'] = 'mysqli';
$db['woodjh']['dbprefix'] = '';
$db['woodjh']['pconnect'] = FALSE;
$db['woodjh']['db_debug'] = TRUE;
$db['woodjh']['cache_on'] = FALSE;
$db['woodjh']['cachedir'] = '';
$db['woodjh']['char_set'] = 'utf8';
$db['woodjh']['dbcollat'] = 'utf8_general_ci';
$db['woodjh']['swap_pre'] = '';
$db['woodjh']['autoinit'] = TRUE;
$db['woodjh']['stricton'] = FALSE;

//--- RCkehoe Configuration
$db['owws']['hostname'] = 'localhost';
$db['owws']['username'] = 'psycon';
$db['owws']['password'] = 'psycon565';
$db['owws']['database'] = 'psycon';
$db['owws']['dbdriver'] = 'mysql';
$db['owws']['dbprefix'] = '';
$db['owws']['pconnect'] = FALSE;
$db['owws']['db_debug'] = TRUE;
$db['owws']['cache_on'] = FALSE;
$db['owws']['cachedir'] = '';
$db['owws']['char_set'] = 'utf8';
$db['owws']['dbcollat'] = 'utf8_general_ci';
$db['owws']['swap_pre'] = '';
$db['owws']['autoinit'] = TRUE;
$db['owws']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */