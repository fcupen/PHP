<?php 

class blog_mo extends CI_Model {

	function getEntries(){
		$this->db->order_by('date DESC');
		return $this->db->get('entries')->result();
	}

	function insert($table, $data){
		return $this->db->insert($table, $data);
	}

	function validate_credentials($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);

		return $this->db->get('users')->row();
	}

	function getEntry($id){
		$this->db->where('id', $id);

		return $this->db->get('entries')->row();
	}

	function getComments($id){
		$this->db->where('id_blog', $id);

		return $this->db->get('comments')->result();
	}
	
	function getMyEntries($username)
	{
	    $this->db->select('id');

	    $this->db->where('author', $username);
        $res = $this->db->get('entries')->result();
        
        $results = array();
        if (!empty($res)) {
            foreach ($res as $k => $v) {
                array_push($results, $v->id);
            }    
        }
        
		return $results;
	}
	
	function getEntryData($entry)
	{
	    $this->db->where('id', $entry);
	    $q = $this->db->get('entries');
	    
	    return $q->row();
	}
	
	function updateEntry($id, $data)
	{
	    $this->db->where('id', $id);
	    
	    return $this->db->update('entries', $data);
	}
	
	function deleteEntry($id)
	{
	    return $this->db->delete('entries', array('id' => $id)); 
	}
}
