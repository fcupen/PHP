<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Search_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
function insert($table, $data){
		return $this->db->insert($table, $data);
	}

//Function: SEARCH ARTICLES OR AUTHORS
	function new_search($fields)
	{
		//GET THE INPUT DATA
		$and_or = $this->input->post('content') != '' ? ' AND ' : ' OR ';
		$search = $this->input->post('search_menu') != '' ? true : false;
		$terms = array();
		foreach($fields as $field){
			if($this->input->post($field) && $this->input->post($field) != '') 
			{
				$terms[] = "$field LIKE '%" . $this->input->post($field) . "%'";
	        }
		}
		if($search == true){$terms[] = "AND (title LIKE '%" . $this->input->post('search_menu') . "%' OR content LIKE '%" . $this->input->post('search_menu') . "%')";};
		
		
		//DB
		$sql = "SELECT"; 
		$sql .= " entries.id,entries.cover,users.username,users.profile,users.imagen,entries.title, entries.content,entries.author";
		$sql .= " FROM entries,users  ";
		if(count($terms) > 0) 
		{
			//WHERE CONDITIONS
		    $sql .= "WHERE entries.author = users.id AND " . implode ($and_or, $terms);
		}else{
			//WHERE CONDITIONS
			$sql .= "WHERE entries.author = users.id ";};
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}else{
			return FALSE;
		}
	}
//Function: RANDOM ARTICLE
	function random_article()
	{
		$this->db->select('users.name, category.name_cat,entries.hide,entries.id,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('entries');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->order_by('RAND()');
		$this->db->where('approved', 'y');
		$this->db->where('hide', 'no');
		$this->db->join('users', 'users.id = entries.author');
		$this->db->limit('1');
		$query2 = $this->db->get();
		
		return $query2->row();
	}
}
