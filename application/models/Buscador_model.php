<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Buscador_model extends CI_Model
{
	
	function __construct()
	{
		
		parent::__construct();
		
	}
function insert($table, $data){
		return $this->db->insert($table, $data);
	}
	function nueva_busqueda($campos)
	{
		
		
		$and_or = $this->input->post('content') != '' ? ' AND ' : ' OR ';
			
		$condiciones = array();
		
		
		foreach($campos as $campo){
			
			
			if($this->input->post($campo) && $this->input->post($campo) != '') 
			{
				
				$condiciones[] = "$campo LIKE '%" . $this->input->post($campo) . "%'";
				
	        }
			
		}
			

		$sql = "SELECT * FROM entries ";
		

		if(count($condiciones) > 0) 
		{
			
		    $sql .= "WHERE " . implode ($and_or, $condiciones);
			
		}
 
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
				
			return $query->result();
				
		}else{
			
			return FALSE;
			
		}
		
	}
	
}
