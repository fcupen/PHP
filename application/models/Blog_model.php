<?php 

class blog_model extends CI_Model {

//Function: GET INFORMATION FOR THE EMAIL
	function email_data($id){
		$this->db->select('*'); 
		$this->db->from('email');
		$this->db->where('type', $id);
		$res = $this->db->get();

	return $res->row();
	}


//Function: GET INFORMATION FOR THE EMAIL
	function random_article(){
		$this->db->select('*'); 
		$this->db->from('entries');
		$this->db->order_by('RAND()');
		$this->db->where('approved', 'y');
		$this->db->where('hide', 'no');

		$res = $this->db->get();

	return $res->row();
	}

//Function: GET THE ID WITH THE KEY FROM A EMAIL
	function verif($key){
		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('verif', $key);
		$res = $this->db->get();

	return $res->row();
	}

//Function: GET THE ID WITH THE USERNAME
	function username_id($user){
		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('username', $user);
		$res = $this->db->get();

	return $res->row();
	}
//Function: SIDE MENU RELATED BAR
	function side_menu(){
		$this->db->select('*'); 
		$this->db->from('side_menu');
		$res = $this->db->get();

	return $res->result();
	}

//Function: FOOTER
	function footer(){
		$this->db->select('*'); 
		$this->db->from('footer');
		$this->db->where('title', 'buttons');
		$this->db->where('footer_id', NULL);
		$res = $this->db->get();

	return $res->result();
	}
	function footer_content($aux){
		$this->db->select('*'); 
		$this->db->from('footer');
		$this->db->where('title', 'buttons');
		$this->db->where('footer_id', $aux);
		$res = $this->db->get();

	return $res->result();
	}
	function footer_social(){
		$this->db->select('*'); 
		$this->db->from('footer');
		$this->db->where('title', 'social');
		$res = $this->db->get();

	return $res->result();
	}

//Function: CENTRAL MENU
	function central_menu(){
		$this->db->select('*'); 
		$this->db->from('central_menu');
		$res = $this->db->get();

	return $res->result();
	}

//Function: GET THE USERNAME WITH THE ID
	function id_username($id){
		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('id', $id);
		$res = $this->db->get();

	return $res->result();
	}

//Function: GET THE URL FOR THE RECOVER PASSWORD
	function recover_password($url){
		$this->db->select('*'); 
		$this->db->from('recover_password');
		$this->db->where('url', $url);
		$res = $this->db->get();

	return $res->row();
	}

//Function: GET THE CATEGORIES
	function categ(){
		$res = $this->db->get('category')->result();

	return $res;
	}

//Function: GET CATEGORIES WITH ID "DESC"
	function categ2(){
		$this->db->select('*'); 
		$this->db->from('category');
		$this->db->order_by('id DESC');
		
		$res = $this->db->get();

	return $res->row();
	}

//Function: GET INFO FOR THE "FEATURED AUTHOR"
	function featuredAuthor(){
		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('profile', 'y');
		$this->db->limit('1');	
		$query = $this->db->get();
		
		return $query->row();
		
	}

//Function: GET INFO FOR THE "FAVORITE AUTHORS"
	function favAuthor($user){
		$this->db->select('*'); 
		$this->db->from('fav');
		$this->db->join('users', 'users.id = fav.author');
		$this->db->where('user', $user);
		$query = $this->db->get();
		return $query->result();
		
	}

//Function: GET INFO FOR THE "FAVORITE ARTICLES"
	function favArticle($article){
		$this->db->select('*'); 
		$this->db->from('like');
		$this->db->join('entries', 'entries.id = like.article');
		$this->db->join('users', 'users.id = entries.author');
		$this->db->where('user', $article);
		$query = $this->db->get();
		return $query->result();
		
	}

//Function: GET ARTICLES FOR THE "FEATURED AUTHOR" 
	function featuredAuthorRow(){
		$this->db->select('category.name_cat,entries.id,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('users');
		$this->db->join('entries', 'users.id = entries.author');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->order_by('sort ASC');
		$this->db->where('hide', 'no');
		$this->db->where('profile', 'y');
		$this->db->where('approved', 'y');
		$this->db->limit('2');	
		$query = $this->db->get();
		
		return $query->result();
		
	}

//Function: GET INFO FOR THE "FEATURED AUTHOR"
	function featuredAuthorRow2(){
		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->join('user_info', 'users.id = user_info.user');
		$this->db->where('profile', 'y');

		$query = $this->db->get();
		
		return $query->row();
		
	}

//Function: GET "locked" ARTICLES ; MAX 10
	function getEntries2($locked){
		$this->db->select('users.name, category.name_cat,entries.id,entries.hide,entries.visits,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('entries');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->where('hide', 'no');
		$this->db->order_by('sort ASC');
		$this->db->where('locked', $locked);
		$this->db->join('users', 'users.id = entries.author');
		$this->db->where('approved', 'y');
		$this->db->limit('10');	
		$query = $this->db->get();
		return $query->result();
		
	}

//Function: GET EMAIL CONTENT
	function getEmail($id){
		$this->db->where('type', $id);
		$res = $this->db->get('email')->result();
		foreach ($res as $k => $v) {
			$result = $v->message;	
		};
		return $result;
	}

	
//Function: GET CATEGORIES
	function getCategory($aux){
		$this->db->select('category.name_cat,entries.id,entries.hide,entries.visits,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('entries');
		$this->db->order_by('sort ASC');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->where('category', $aux);
		$this->db->where('hide', 'no');
		$this->db->where('approved', 'y');
		$query = $this->db->get();
		return $query->result();
		
	}
	
//Function: GET CATEGORIES
	function getCat($aux){
		$this->db->select('*'); 
		$this->db->from('category');
		$this->db->where('id', $aux);
		$query = $this->db->get();
		return $query->row();
		
	}
//Function: GET ARTICLES LESS THAN 10
	function getEntries($entry){
		$query = $this->db->query("SELECT * FROM entries WHERE locked = 'yes'");
		$aux=$query->num_rows();
		$aux2=10-$aux;

		$this->db->select('users.name, category.name_cat,entries.hide,entries.visits,entries.id,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('entries');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->order_by('RAND()');
		$this->db->where('approved', 'y');
		$this->db->where('hide', 'no');
		$this->db->join('users', 'users.id = entries.author');
		$this->db->limit($aux2);
		$this->db->where('locked', $entry);
		$query2 = $this->db->get();
		return $query2->result();
		
	}

//Function: INSERT DATA
	function insert($table, $data){
		return $this->db->insert($table, $data);
	}

//Function: VALIDATE DATA
	function validate_credentials($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);

		return $this->db->get('users')->row();
	}

//Function: FIND ARTICLES WITH THE USER ID
	function findUser($id){
		$this->db->where('author', $id);
		$this->db->order_by('date ASC');
		$res = $this->db->get('entries')->result();
		foreach ($res as $k => $v) {
			$result = $v->title;	
		};
		return $result;
	}

//Function: 
	function findUser2($id){
		$this->db->where('author', $id);
		$this->db->order_by('date ASC');
		$res = $this->db->get('entries')->result();
		foreach ($res as $k => $v) {
			$result = $v->approved;	
		};
		return $result;
	}

//Function: GET ARTICLES WITH THE ID
	function getEntry($id){
		$this->db->select('*'); 
		$this->db->from('entries');
		$this->db->where('entries.id', $id);
		$this->db->join('users', 'users.id = entries.author');		
		//$this->db->join('category', 'category.id = entries.category');


			$titl=$this->db->get();
		return $titl->row();
	}

//Function: GET LIKE
	function getLike($id, $id2){
		$this->db->select('*'); 
		$this->db->from('like');
		$this->db->where('article', $id);
		$this->db->where('user', $id2);
		$titl=$this->db->get();
		return $titl->row();
	}


//Function: GET FAV
	function getFav($id, $id2){
		$this->db->select('*'); 
		$this->db->from('fav');
		$this->db->where('user', $id);
		$this->db->where('author', $id2);
		$titl=$this->db->get();
		return $titl->row();
	}

//Function: GET COMMENTS
	function getComments($id){
		$this->db->where('id_article', $id);

		return $this->db->get('comments')->result();
	}
	
//Function: GET "MY" ARTICLES
	function getMyEntries($username)
	{
	    $this->db->select('users.name,category.name_cat,entries.id,entries.visits,entries.hide,entries.title,entries.cover, entries.author,entries.content,entries.date,entries.pay,entries.tags,entries.approved'); 
		$this->db->from('entries');
		$this->db->join('category', 'category.id = entries.category');
		$this->db->order_by('date DESC');
		$this->db->join('users', 'users.id = entries.author');
		$this->db->where('entries.author', $username);
		$query2 = $this->db->get();
		return $query2->result();
	
        
		
	}

//Function: APPROVE ARTICLES, WITH THE RESERVED WORDS
	function getApproval($content)
	{
		$this->db->select('*');
		$res = $this->db->get('approval')->result();
		$query2 = $content;
		//FIND SOME LINK
		$porc2 =explode('<a', $query2);
		$porc3 =explode('href', $query2);
		//CHECK FOR LINKS
        $arr_length2 = count($porc2);
        $arr_length3 = count($porc3);
        if($arr_length2 > '1'){$aux++;};
        if($arr_length3 > '1'){$aux++;};
        

		foreach ($res as $k => $v) {
			//COUNTING WORDS
			$query = $content;
        $porc =explode(' ', $query);
        $arr_length = count($porc);
        $results = array();
        for($i=0;$i<$arr_length;$i++)
        {
        	//CHECK WORDS
        	if( $porc[$i] == $v->word){$aux++;
        		array_push($results, $porc[$i]);
        	};

        };
		};
		return $aux;	
	}

//Function: GET CATEGORIES
	function get_base_categories(){
	   $this->db->where('user', '');
	   $this->db->where('category_id', '0');
        $res = $this->db->get('category')->result();

	return $res;
	}

//Function: GET INFO FOR THE USER
	function getinfo_user($username){
	    $this->db->where('user', $username);
        $res = $this->db->get('user_info')->row();

	return $res;
	}

//Function: GET INFO FOR THE USER
	function getinfo_user2($username){
	$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('id', $username);
        $res = $this->db->get()->row();

	return $res;
	}

//Function: GET INFO FOR THE USER
	function getinfo_user2b($username){
	$this->db->select('*'); 
		$this->db->from('user_info');
		$this->db->where('user', $username);
        $res = $this->db->get()->row();

	return $res;
	}

//Function: GET INFO FOR THE USER
	function getMyData($username)
	{
		$as='0';
	    $this->db->where('id', $username);
	    $this->db->limit($as);
        $res = $this->db->get('users')->row(); 

		return $res;
	}

//Function: GET INFO FOR THE ARTICLE
	function getEntryData($entry)
	{
		$this->db->select('*'); 
	    $this->db->where('id', $entry);
	    $q = $this->db->get('entries');
	    
	    return $q->row();
	}
	
//Function: UPDATE ARTICLE
	function updateEntry($id, $data, $db)
	{
	    $this->db->where('id', $id);
	    
	    return $this->db->update($db, $data);
	}

//Function: UPDATE PROFILE
	function updateUser($id, $data)
	{
	    $this->db->where('id', $id);
	    
	    return $this->db->update('users', $data);
	}

//Function: UPDATE URL FOR THE RECOVER
	function updateurl($id, $data)
	{
	    $this->db->where('username', $id);
	    
	    return $this->db->update('recover_password', $data);
	}

//Function: UPDATE INFO
	function updateUser2($id, $data)
	{
		$query = $this->db->query("SELECT * FROM user_info WHERE user = '".$id."'");
		$aux=$query->num_rows();
		if($aux == '0'){
			return $this->db->insert('user_info', $data);
		}else{
			$this->db->where('user', $id);
			return $this->db->update('user_info', $data);
		};
	}

//Function: GET BANNER INFO
	function getReports($url)
	{
		$this->db->select('*'); 
	    $this->db->from('invoice');
	    $this->db->order_by('id DESC');
	    $this->db->where('author', $url);
	    $query2 = $this->db->get();
		return $query2->result();
	}

//Function: GET BANNER INFO
	function getBanner($url)
	{
		$this->db->select('*'); 
	    $this->db->from('banner');
	    $this->db->join('banner_position', 'banner_position.id = banner.position');
	    $this->db->join('banner_page', 'banner_page.id = banner.page');
	    $this->db->order_by('sort ASC');
	    $this->db->where('url', $url);
	    $query2 = $this->db->get();
		return $query2->result();
	}

//Function: GET BANNER INFO FOR ALL PAGES
	function getBannerAll($url)
	{
		$this->db->select('*'); 
	    $this->db->from('banner');
	    $this->db->join('banner_position', 'banner_position.id = banner.position');
	    $this->db->join('banner_page', 'banner_page.id = banner.page');
	    $this->db->order_by('sort ASC');
	    $this->db->where('url', $url);
	    $query2 = $this->db->get();
		return $query2->result();

	}
	
//Function: DELETE ARTICLE
	function deleteEntry($id)
	{
	    return $this->db->delete('entries', array('id' => $id)); 
	}
//Function: FAV
	function fav($id,$id2)
	{
	    $this->db->select('*'); 
	    $this->db->from('fav');
	    $this->db->where('user', $id2);
	    $this->db->where('author', $id);
	    $query2 = $this->db->get();
		return $query2->row();
	}
//Function: DELETE FAV
	function deleteFav($id)
	{
	    return $this->db->delete('fav', array('id' => $id)); 
	}

	//Function: Like
	function like($id,$id2)
	{
	    $this->db->select('*'); 
	    $this->db->from('like');
	    $this->db->where('user', $id2);
	    $this->db->where('article', $id);
	    $query2 = $this->db->get();
		return $query2->row();
	}
//Function: DELETE LIKE
	function deleteLike($id)
	{
	    return $this->db->delete('like', array('id' => $id)); 
	}
}
