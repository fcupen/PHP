<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Change Password</title>	
	<?php include('style.php');?>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<?php include('menu.php');?>

<br /><br /><br /><br /> 
    <div id="container">
    <div class="container">

      
      <div class=""><form class="form-signin"  action="<?php echo base_url().'users/update_password2/';?>" method="POST">
        
        <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Please insert your new password</div>
        <?php echo (isset($error)) ? '<p>Incorrect Password!</p>' : '';?>
        <label for="password" class="sr-only">New Password</label>
        <input type="password" id="password" name="password" class="form-control"  required autofocus>
        <input type="hidden" id="id" name="id" class="form-control" value="<?=$pass->username?>">
        <label for="password2" class="sr-only">Repeat New Password - </label>
        <input type="password" id="password2" name="password2" class="form-control"  required autofocus>

        <button class="btn btn-lg btn-primary btn-block" type="submit" id="submit">Chage Password</button>
      </form>
      </div>

    </div>
 </div>
<?php include('footer.php');?>

</body>
</html>