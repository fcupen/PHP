<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <style type="text/css">#slide {
  height: 40vw;
  background-image: url(<?=base_url().'public/img/astral-foundations_backg.jpg'?>);
  background-position: none center;
  background-repeat: no-repeat;
  background-size: cover;
  background-color:#464646;
  
}
#row_auth #author #title {
  height: 100px;
  text-align: center;
  font-weight: bolder;
  letter-spacing: 0.2em;
    font-size: 22px;
  font-weight: bolder;
  padding-top: 15px;
}
#row_auth #author #alg_profile #profile #text_profile #data #nam {
  font-size: 22px;
  font-weight: bolder;
}
#row_auth #author #alg_profile #profile #text_profile #data #bio {
  font-size: 18px;
  color: #666;
}
#row_auth #author #alg_profile   #articlesl #titl {
  font-weight: bolder;
  padding-top: 10px;
  padding-right: 0px;
  padding-bottom: 0px;
  padding-left: 0px;

}
#row_auth #author #alg_profile #profile #text_profile {
  float: left;
  width: 900px;
  padding-right: 0px;
  padding-left: 30px;
}
#row_auth #author #alg_profile  #articlesl #row #article #art_img {
  float: left;
  padding-top: 25px;
  padding-right: 25px;
  padding-left: 25px;
}
#row_auth #author #alg_profile  #articlesl #row #article {
  width: 430px;
  height: 90px;
  float: left;
}
#row_auth #author #alg_profile #profile #text_profile #data_2 {
  text-align: right;
  padding-top: 20px;
  margin-top: 20px;
  float: right;
  width: 300px;
  padding-right: 30px;
  font-size: 14px;
  letter-spacing: 0.1em;
}
#row_auth #author #alg_profile  #articlesl #row {
  width: auto;
}
#row_auth #author #alg_profile  #articlesl{
  padding-bottom: 20px;
  padding-top: 0px;
}
#row_auth #author #alg_profile  #articlesl #row #article #art_data {
  float: left;
  padding-top: 20px;
  font-size: 16px;
  font-weight: bolder;
  color: #039;
}
#row_auth #author #alg_profile #profile #img {
  float: left;
  width: 260px;
}
#row_auth #author #alg_profile #profile {
  float: none;
  height: 520px;
  width: 1200px;
  padding-top: 10px;
  padding-bottom: 10px;
  margin-right: auto;
  margin-left: auto;
}
#row_auth #author {
 /* background-image: url(img/astral-foundations_featured-bg.jpg);*/
  background-repeat: repeat;
  height: auto;
    margin-top:0px;
  margin-bottom:40px;
  padding-bottom: 40px;
  marker-offset:0px;
  margin: 0 auto;
  overflow: hidden;
  width: 1200px;
}
#row_auth #author #alg_profile {
  height: auto;
  width: 100%;
}
@media screen and (max-width:1200px){
 #row_auth  #author{
width: 800px;
    height: auto;
    padding-bottom:30px;

  }
 #row_auth  #author #alg_profile #profile {
    float: none;
    height: auto;
    width: 810px;
    padding-bottom:30px;
  }
 #row_auth  #author #alg_profile #profile #text_profile {
    float: left;
    width: 500px;
    padding-bottom:30px;
  }
 #row_auth  #author #alg_profile  #articlesl {
    width: 250px;
    height: auto;
    float: left;
    padding-bottom:30px;
    /*display:none;*/
  }

}
@media screen and (max-width:1000px){
#row_auth #author #title {
  height: 150px;
  text-align: center;
}
#row_auth #author #title {

  padding-top: 75px;
}
}
@media screen and (max-width:800px){
  #row_auth #author #title {
  height: 100px;
  padding-bottom: 65px;
}
#row_auth {
  }
 #row_auth  #author{
    height: auto;
      background-repeat: repeat;
      width: 100%;
  }
  #row_auth #author #alg_profile{
margin-right: auto;
    padding-left: 25px;
    padding-right: 25px;
    margin-left: auto;
    width: 100%;
    text-align: center;
  }
  #row_auth #author #alg_profile #profile {
    float: left;
    height: auto;
    width: 100%;
    text-align: center;
    margin-right: auto;
    padding: 0 0 0 0;
    margin-left: auto;
  }
  #row_auth #author #alg_profile #profile #img{
    float: none;
    text-align: center;
    padding-bottom: 25px;
    padding-top: 25px;
    width: 100%;
  }
  #row_auth #author #alg_profile #profile #text_profile {
    float: none;
    width: 100%;
    margin-right: auto;
    padding: 0 0 0 0;
    margin-left: auto;
    text-align: center;
  }
  #row_auth #author #alg_profile  #articlesl {
    width: 90%;
    height: auto;
    display:block;
    float: none;
    overflow: hidden;
  }
  #row_auth #author #alg_profile  #articlesl  #titl {
    width: 100%;
    height: auto;
    display:block;
    float: none;
    overflow: hidden;
  }

}
@media screen and (max-width:400px){
  #row_auth #author #alg_profile{
    width: 350px;
  }
  #row_auth #author #alg_profile #profile {
    width: 350px;
  }
  #row_auth #author #alg_profile #profile #text_profile {
    float: none;
    width: 100%;
    text-align: center;
    padding: 0 0 0 0;
  }
  #row_auth  #author{
    height: auto;
      background-repeat: repeat;
      width: 350px;
      padding: 0 0 0 0;
  }
  #row_auth {
      padding: 0 0 0 0;
  }
    #row_auth #author #alg_profile {
      padding: 0 0 0 0;
  }
  #row_auth #author #alg_profile #profile #img{
    float: none;
    text-align: center;
    width: 100%;
  }
#row_auth #author #alg_profile #profile #text_profile #data #nam {
  padding-top:20px;
}
#row_auth #author #alg_profile  #articlesl #row #article{
  height:auto;
  width:100%;
}
#row_auth #author #alg_profile  #articlesl #row #article #art_img {
  float: none;
  width:100%;
  
}
#row_auth #author #alg_profile  #articlesl #row #article #art_img img{
  float: none;
  width:90%;
  padding: 0 0 0 0;
  height: auto;
  
}
#row_auth #author #alg_profile  #articlesl #row #article #art_data {
  float: none;
padding-left: 15px;
padding-right: 15px;
}

}
@media screen and (max-width:350px){
 #row_auth #author #alg_profile{
    width: 300px;
  }
  #row_auth #author #alg_profile #profile {
    width: 300px; 
  }
  #row_auth #author #alg_profile #profile #text_profile {
    float: none;
    width: 100%;
    text-align: center;
    padding: 0 0 0 0;
  }
  #row_auth  #author{
    height: auto;
      background-repeat: repeat;
      width: 300px;
  }
  #row_auth #author #alg_profile #profile #img{
    float: none;
    text-align: center;
    width: 100%;
  }
#row_auth #author #alg_profile #profile #text_profile #data #nam {
  padding-top:20px;
}
#row_auth #author #alg_profile  #articlesl #row #article{
  height:auto;
  width:100%;
}
#row_auth #author #alg_profile  #articlesl {
  margin-right: auto;
  margin-left: auto;padding: 0 0 0 0;
}
#row_auth #author #alg_profile  #articlesl #row #article{
  margin-right: auto;text-align: center;
  margin-left: auto;padding: 0 0 0 0;
}
#row_auth #author #alg_profile  #articlesl #row{
  margin-right: auto;text-align: center;
  margin-left: auto; padding: 0 0 0 0;
}
#row_auth #author #alg_profile  #articlesl #row #article #art_img {
  float: none; margin-right: auto;
  margin-left: auto;text-align: center;
  width:100%;padding: 0 0 0 0;
  
}
#row_auth #author #alg_profile  #articlesl #row #article #art_img img{
  float: none;
  width:300px;
  text-align: center;
  
}
#row_auth #author #alg_profile  #articlesl #row #article #art_data {
  float: none;padding: 0 0 0 0;

}
#row_auth #author #alg_profile #data_2{
 padding-bottom: 10px;
 padding-top: 10px;

}

}

</style>
</head>
<body>
	<?php  include('menu.php');?>


  
    <!-- MENU --> 
<div id="row_auth">

<div id="author">
  <div id="title" >
      <div style="padding-top:10px;" >
      <p>FEATURED AUTHORS </p></div> 
</div>
  <div id="alg_profile" > <!-- FEATURED AUTHOR-->
<div id="profile" style="height: auto;overflow: hidden;" >
    <div id="img">
   <a href="<?=base_url().'users/profileauthor/'.$featured->id.'/'.$featured3->id?>"> <img src="<?=base_url().'public/img/'.$featured->imagen?>" width="260" height="260" /></a>
    </div>
    <div id="text_profile" style="height: auto;">
      <div id="data" style="width:100%; float:none;">
        <div id="nam"><a href="<?=base_url().'users/profileauthor/'.$featured->id.'/'.$featured3->id?>"><?=$featured->name?></a></div>
        <div id="bio"><?php
            $query2 = $featured3->bio;
    $porc2 =explode(".", $query2);
    
            echo $porc2['0'];?></div>
      </div>
        
    </div>
    </div><div id="articlesl" style="float:none; ">
        <div id="titl" >
        <strong> LINKS TO TOP ARTICLES</strong>
        </div>
          <div id="row"   style="padding-top: 5px;">

                

          <?php foreach($featured2 as $pro) : ?>
              <div id="article" style="float:none; width:100%;overflow: hidden;padding-top: 5px;">
               <div id="art_img" style="float:left; padding-bottom: 15px;padding-left: 5px; padding-right: 25px;"> 
               <a href="<?=base_url().'users/view/'.$pro->id?>"><img src="<?=base_url().'public/articles/'.$pro->cover?>" width="70" height="70" /></a>
               </div>
               <div id="art_data" style="float:left;padding-bottom: 15px;overflow: hidden; width: calc(80% - 50px); text-align:left;"><a href="<?=base_url().'users/view/'.$pro->id?>"><strong><?=$pro->title?></strong></a>
               </div>
               </div>
            <?php endforeach; ?>
              
            </div>
        </div>
        <div id="data_2"><div class="button">
       <a href="<?=base_url().'users/profileauthor/'.$featured->id.'/'.$featured3->id?>"> <font style="color:white;text-align: center;">VIEW MORE ARTICLES</font></a>
        </div></div>

<!-- FEATURED AUTHOR-->

    </div>
    
    </div></div>
</div>

<div id="panel" > 
    <div id="btn_panel" >
    
<div id="aln_panel">
<!-- CENTRAL MENU-->
<?php  include('formula/central_menu.php'); ?>

<!-- CENTRAL MENU-->
</div>

    </div>
</div>

<!-- FOOTER -->
<?php include('footer.php');?>

 
</body>
</html>
