<!DOCTYPE html>
<html>
	
	<head>
		
		<meta charset="UTF-8" />
		<title><?php echo $titulo ?></title>
		<?php include('style.php');?>	
		<link rel="stylesheet" href="<?php echo base_url('css/estilos.css?'.time()) ?>" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url('css/960.css?'.time()) ?>" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url('css/reset.css?'.time()) ?>" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url('css/text.css?'.time()) ?>" type="text/css" media="screen" />
		<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'></script>
        <script type="text/javascript" src="<?php echo base_url('js/funciones.js?'.time()) ?>"></script>
	</head>
	
<body>
<div class="container_12">
	<div  class="form-inline" id="buscador_multipe">
		<h2>Search</h2>
		<?php $atributos = array('class' => 'formulario') ?>
		<?php echo form_open('buscador',$atributos) ?>
 
			<?php echo form_label('Content') ?>
			<input type="text" name="content" class="form-control" id="content" placeholder="Search content of the articles" />
			
			<?php echo form_label('Title') ?>	
			<input type="text" autocomplete="off" onpaste="return false" name="title" 
			id="title" class="form-control" placeholder="Search Title of the Articles" />
			
			<!--este es nuestro autocompletado-->
			<input type="text" autocomplete="off" onpaste="return false" name="author" 
			id="author" class="form-control" placeholder="Search Users" />
			
            <div class="muestra_poblaciones"></div>
				
			<?php echo form_submit('buscar','Search') ?>
			
		<?php echo form_close() ?>
		
	</div>	
			
	<?php //si hay resultados los mostramos
 
	if(is_array($resultados) && !is_null($resultados))
	{
	?>
	<div class="grid_12 resultados">
		<h2>Results</h2>

		

		<div class="col-md-6" id="body_resultados">

		<table class="table table-hover">
<tr><td>Title</td><td>Author</td><td>Content</td></tr>
		<?php
		foreach($resultados as $fila)
		{
		?>
			<tr><td><?php echo $fila->title ?></td>
			<td><?php echo $fila->author ?></td>
			<td><?php echo $fila->content ?></td>
				</tr>
		<?php
		}
		?>

		</table>
		</div>
	</div>
	<?php
	}
	?>	
</div>
</body>
</html>