<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sign In</title>	
  <script src='https://www.google.com/recaptcha/api.js'></script>
	<?php include('style.php');?>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
</head>
<body>
	<?php include('menu.php');?>
	<br /><br /><br /><br />
	

<div>

      <form  id="contactForm" class="form-horizontal" action="<?php echo base_url().'users/validate/';?>" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php echo (isset($error)) ? '<p>This Username/Password was incorrect!</p>' : '';?>

        <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="username" name="username"  placeholder="Email"/>
                </div>
            </div>
<div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-6">
                    <input  type="password" id="password"name="password" class="form-control" placeholder="Password"  />
                </div>
            </div>
        <div class="form-group">
        <div class="col-md-3 control-label">

          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> 
        <div class="col-md-3 control-label">

          <label>
            <a href="<?=base_url().'users/recover/'?>">Recover Password</a>
          </label>
        </div> 
        </div>
         
        <div>
          <div class="form-group">
         <div class="col-md-3 control-label">
        <div class="g-recaptcha" data-sitekey="6Ldh8BYTAAAAAJiD7_oqxFMLByX6unUoyRtxKZcz"></div>
        </div>
        </div>
        </div>
  <div class="form-group">
        <div class="col-md-3 col-md-offset-3">
            <div id="messages"></div>
        </div>
    </div>
   
  <div class="form-group">
        <div class="col-md-3 col-md-offset-3">
        <button class="btn btn-primary" type="submit" >Sign in</button>
        </div>
    </div>
      </form>


    </div>
<script type="text/javascript">$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>



<?php include('footer.php');?>

</body>
</html>