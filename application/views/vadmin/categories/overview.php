
<div class="container">

  <div >
    <ul class="nav navbar-nav">
      <li><a href="./">INICIO</a></li>
      <li><a href="./001"><i class="glyphicon glyphicon-plus"></i> NUEVA</a></li>
    </ul>
  </div>
</div>

	<div class="container">
	<div class="row">
	<div class="col-md-12">
	<h1>CATEGORIAS</h1>

<?php if(count($categories)>0):?>
	<ul>
	<?php foreach($categories as $cat):?>
		<li><?php echo $cat["name"]." ".edit_btn($cat["id"])." ".del_btn($cat["id"]);?> </li>
		<?php
		list_tree_cat_id($cat["id"]);
		?>
	<?php endforeach;?>
	</ul>
<?php else:?>
	<p class="alert alert-danger">No hay categorias</p>
<?php endif;?>

	</div>
	</div>
	</div>