
<div class="container_12">
	<div  class="form-inline" id="buscador_multipe">
		<h2>Search</h2>
		<?php $atributos = array('class' => 'formulario') ?>
		<?php echo form_open('vmain/buscar/',$atributos) ?>
 
			<?php echo form_label('Content') ?>
			<input type="text" name="content" class="form-control" id="content" placeholder="Search content of the articles" />
			
			<?php echo form_label('Title') ?>	
			<input type="text" autocomplete="off" onpaste="return false" name="title" 
			id="title" class="form-control" placeholder="Search Title of the Articles" />

			<input type="text" autocomplete="off" onpaste="return false" name="author" 
			id="author" class="form-control" placeholder="Search Users" />
			
            <div class="muestra_poblaciones"></div>
				
			<?php echo form_submit('buscar','Search') ?>
			
		<?php echo form_close() ?>
		
	</div>	
			
	<?php 
 
	if(is_array($resultados) && !is_null($resultados))
	{
	?>
	<div class="grid_12 resultados">
		<h2>Results</h2>

		

		<div class="col-md-6" id="body_resultados">

		<table class="table table-hover">
<tr><td>Title</td><td>Author</td><td>Content</td></tr>
		<?php
		foreach($resultados as $fila)
		{
		?>
			<tr><td><?php echo $fila->title ?></td>
			<td><?php echo $fila->author ?></td>
			<td><?php echo $fila->content ?></td>
				</tr>
		<?php
		}
		?>

		</table>
		</div>
	</div>
	<?php
	}
	?>	
</div>
