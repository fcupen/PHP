<script src='https://www.google.com/recaptcha/api.js'></script>
<style type="text/css">
body {
	margin:15px 10px 0;
}
</style>


	<div class='content_area'>
	
		<h2>Registration</h2>
		
		<div style='font-style:italic;margin:15px 10px 0;'>Please enter the following information. Your information will be kept completely confidential. </div>
		
		<div style='margin:15px 10px 0;'>Required fields are indicated with a red asterisk (<span style='color:red;'>*</span>) </div>
		
		<hr />
		
		<form action='/register/submit_email' method='POST'>
		
			<table width='50%' cellPadding='2' cellSpacing='0'>
			
				<tr>
					<td style='width:100px;'><b>Email Address:</b> <span style='color:red;'>*</span></td>
					<td><input type='text'id='email' name='email' value='<?=set_value('email')?>'></td>
				</tr>
				
				<tr>
					<td style='width:100px;'><b>Captcha:</b> <span style='color:red;'>*</span></td>
					<td><div class="g-recaptcha" data-sitekey="6LceAxATAAAAAJyJPqyNm-ewf0sroy1fI8_THSYb"></div></td>
				</tr>
								
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<table width='200'>
							<tr>
								<td valign='top' width='10'><input type='checkbox' name='newsletter' value='1' <?=set_checkbox('newsletter','1',TRUE)?>></td>
								<td><div>I want to receive the newsletter. (Get coupons and special promotions) </div></td>
							</tr>
						</table>
					 </td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<table width='200'>
							<tr>
								<td valign='top' width='10'><input type='checkbox' name='terms' value='1' <?=set_checkbox('terms','1')?>></td>
								<td><div>I have read and agreed to all the Member <a href='/terms' target='_blank'>Terms and Conditions</a> <span style='color:red;'>*</span></div></td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" value="Register" class='btn btn-primary btn-large'></td>
				</tr>
				
			</table>
		
		</form>
		
	</div>