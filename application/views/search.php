<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <style type="text/css">.borderless td, .borderless th {
    border: none;
}

.table-borderless tbody tr td, .table-borderless tbody tr th, .table-borderless thead tr th {
    border: none;
}</style>
</head>
<body>
    <?php include('menu.php');?><div id="container">
    <br /> <br /><!-- MENU -->
<div class="container_12">
    <div  class="form-inline" id="buscador_multipe">
        <div style="    height: 100px;text-align: center;font-weight: bolder; letter-spacing: 0.2em; font-size: 29px; padding-top: 30px;">Search</div>
        
        <!-- SEARCH FORM -->

       <?php $atributos = array('class' => 'formulario') ?>
        <?php echo form_open('users/search_adv/',$atributos) ?>
 
            <?php echo form_label('Content') ?>
            <input type="text" name="content" class="form-control" id="content" placeholder="Search content of the articles" />
            
            <?php echo form_label('Title') ?>   
            <input type="text" autocomplete="off" onpaste="return false" name="title" 
            id="title" class="form-control" placeholder="Search Title of the Articles" />

            <input type="text" autocomplete="off" onpaste="return false" name="username" 
            id="username" class="form-control" placeholder="Search Users" />
            
            
               <div style="padding-top:15px;">
            <?php echo form_submit('search','Search',"class='btn btn-default'") ?>
            <a href="<?=base_url().'users/view/'.$random->id?>"><button type="button" id="button">Random Article</button></a></div> 
        <?php echo form_close() ?>
       <!-- SEARCH FORM -->

    </div>  
            
    <?php 
 
    if(is_array($results) && !is_null($results))
    {
    ?>
    <!-- SEARCH FORM -->
<div class="grid_12 resultados">
        <div  style="    height: 100px;font-weight: bolder; letter-spacing: 0.2em; font-size: 29px; padding-top: 30px;">Results</div>

        

        <div class="" id="body_resultados">

        <table class="table table-hover table-borderless borderless" border="0" style="border: 0px; border-collapse:collapse ">
<!-- SEARCH RESULTS -->
     
<?php
        foreach($results as $field)
        {
        ?>
        <table class="table borderless table-borderless">
            <tr><td width="50"><img src="<?=base_url().'public/articles/'.$field->cover?>" alt="..." class="img-thumbnail" width="50" height="50"></td><td><a href="<?=base_url().'users/view/'.$field->id?>"><?=$field->title ?></td></tr>
            <tr><td  width="50"><img src="<?=base_url().'public/img/'.$field->imagen?>" alt="..." class="img-thumbnail" width="50" height="50"></td><td><?php if($field->profile == 'y'){?><a href="<?=base_url().'users/profileauthor/'.$field->author?>"><?php };?><?=$field->username ?><?php if($field->profile == 'y'){?></a><?php };?></td></tr>
            
               </table>
        <?php
        }
        ?>
<!-- SEARCH RESULTS -->
        </table>
        </div>
    </div>
    <?php
    }
    ?>  
    <!-- SEARCH RESULTS -->
</div>
<!-- FOOTER -->
</div>
</div> 
    <br /><br />

  
<?php include('footer.php');?>

</body>
</html>