<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="<?=base_url().'public/style.css'?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url().'public/style_sub.css'?>" rel="stylesheet" type="text/css" />
<style type="text/css">
.dropdown.dropdown-lg .dropdown-menu {
    margin-top: -1px;
   /* padding: 6px 20px;*/
}
.input-group-btn .btn-group {
    display: flex !important;
}
.btn-group .btn {
    border-radius: 0;
    margin-left: -1px;
}
.btn-group .btn:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}
.form-group .form-control:last-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
    #adv-search {
        width: 500px;
        margin: 0 auto;
    }
    .dropdown.dropdown-lg {
        position: static !important;
    }
    .dropdown.dropdown-lg .dropdown-menu {
        min-width: 500px;
    }
}


    #side_bar {
  -moz-border-radius: 18px;
  -webkit-border-radius: 18px;
  border-radius:18px;
  background-color:rgba(255,255,255,0.7);
  height: auto;
  padding: 0 0 0 0;
}
.titulo{
  font-size: 24px;
  font-weight: bolder;
  color: #FFF;
  background-color: #000;
  height: 40px;
  padding-bottom: 17px;
width: 100%;
padding: 0 0 0 0;
  border-top-left-radius: 18px;
  border-top-right-radius: 18px;
  background-image:url(img/astral-foundations_related-links.jpg);
  background-repeat:repeat-x;
}
.centrar{width: auto;} 
.centrar2{width: 800px;} 
.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
@media screen and (max-width:970px){ 
  #side_bar {
width: 100%;} 
.centrar{width: 100%;} 
.centrar2{width: auto;} 
.menu_bar {
    display:block;
    width:100%;

    z-index:1000; 
    position: fixed;
    top:0;
    padding: 0px;
    background:#fff;
      overflow-x: hidden;
  }

  .menu_bar .bt-menu {margin: 0px;
    padding: 0px;
    background-color: #000;
    height: 45px;
  }
  .menu_bar_btns{
        top:0;
    padding: 0px;
    display: block;width:300px;
    padding: 0px;
    color: #999;height: 100%;
    overflow: hidden;
    text-decoration: none;
      overflow-x: hidden;
      z-index:5;
      /*overflow-y: scroll;*/
  }
  .menu_bar_btns2{
        top:0;
    padding: 0px;
    display: block;width:100%;
    padding: 0px;
    height: 100%;
    
    
      z-index:0;
  }
  header nav {
    background-color:#FFF;
    width: 100%;
    height: 100%;
    z-index:1000;
    position: fixed;
    right:100%;
    margin: 0;
    display: block;
    
  }
  header nav ul{
    height:100%;
  }
  header nav ul li {
    text-decoration: none;
    display: block;
    border-bottom:1px solid rgba(255,255,255,.5);
    height:auto;
  }
  header nav ul li img{
    width:100%;
    height:auto;  
  }
  header nav ul li #search{
    width:100%;

    height:auto;  
  }
  header nav ul li #search #inp{
    float: left;  
    /*border: 2px solid #dadada;
    border-radius: 7px;*/
    float: left;
    padding-top: 3px;
    padding-bottom: 3px;
  }
  header nav ul li #search #inp #content{
  
    border: 0px ;

  }
  header nav ul li #search #srch{
    float: left;
  }

  header nav ul li a {
    color:#000;
    text-decoration: none;
    padding-top:10px;
    display: block;   
  }
 
 
  header nav ul li .caret {
    float: right;
  }

/**/
  
#header {
  height: 2px;  
  display: none;  
  width: 50px;
}
#header #menu {
  display: none;  height: 2px;
}



}


</style>
 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<script>
$(document).ready(main);
 
var contador = 1;
 
function main () {
    $('.menu_bar_btn').click(function(){
        if (contador == 1) {
            $('nav').animate({
                left: '0'
            });
            contador = 0;
        } else {
            contador = 1;
            $('nav').animate({
                left: '-100%'
            });
        }
    });
    

}</script>

