<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Change Password</title>  
  <?php include('style.php');?>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<?php include('menu.php');?>

<br /><br /><br /><br />
    <div id="container">
    <div class="container">

      
      <div class=""><form class="form-signin"  action="<?php echo base_url().'users/recover_password/';?>" method="POST">
        
        <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Please insert your Email</div>
        <?php echo (isset($error)) ? '<p>Incorrect Data!</p>' : '';?>
        <label for="username" class="sr-only">Email</label>
        <input type="email" id="username" name="username" class="form-control"  required autofocus>
        <div style="text-align: center; width:100%; padding-top: 20px;"><button  name="button" id="button" type="submit">Recover</button></div>
        
      </form>
      </div>

    </div> </div>

<?php include('footer.php');?>

</body>
</html>