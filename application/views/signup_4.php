
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <?php include('style.php');?>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script> 
  <script type="text/javascript" src="public/validator.js"></script>   
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

</head>
<body>
  <?php include('menu.php');?>
  <!-- MENU -->

<div id="slide">  
<div id="author">
<div id="link"><div id="text"><a href="https://www.astral-foundations.com/"><font style="color:white;"> Home</font></a> > <strong>Sign Up</strong></div></div>
    
    <div id="alg_profile" >
  
    
    <div id="profile" >
  
<div>
<?php if(isset($error)){echo $error;}?>
<?php foreach ($success as $item => $value) {
  echo '<li>'.$item.' - '.$value.'</li>';
}?>
<form  id="contactForm" data-toggle="validator"  action="<?=base_url().'users/register/';?>" method="POST" enctype="multipart/form-data"> 
  <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Sign Up</div>
  
  <?php echo (isset($error)) ? '<p><font style="color:red;">Please Check the captcha!</font></p>' : '';?>
    
  <div class="form-group">
    <label for="username">Email address</label>
    <input type="email" class="form-control" id="username" name="username" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
<div class='form-group'>
    <label for='first_name'>First Name</label>
    <input type='text' class='form-control' id='first_name' name='first_name' placeholder='First Name'  required>
  </div>
  <div class='form-group'>
    <label for='last_name'>Last Name</label>
    <input type='text' class='form-control' id='last_name' name='last_name' placeholder='Last Name' required>
  </div>
  <div class='form-group'>
    <label for='address_1'>Address Line 1</label>
    <input type='text' class='form-control' id='address_1' name='address_1' placeholder='Address Line 1'>
  </div>
  <div class='form-group'>
    <label for='address_2'>Address Line 2</label>
    <input type='text' class='form-control' id='address_2' name='address_2' placeholder='Address Line 2'>
  </div>
  <div class='form-group'>
    <label for='city_town'>City/Town</label>
    <input type='text' class='form-control' id='city_town' name='city_town'placeholder='City/Town'>
  </div>
  <div class='form-group'>
    <label for='state_province'>State/Province</label>
    <input type='text' class='form-control' id='state_province' name='state_province' placeholder='State/Province'>
  </div>
  <div class='form-group'>
    <label for='zip_postal'>Zip/Postal</label>
    <input type='text' class='form-control' id='zip_postal' name='zip_postal' placeholder='Zip/Postal'>
  </div>
  <div class='form-group'>
                      <label for='country'>Country</label>
                     <select style='width:auto;' name='country' class='form-control'  required><option >Select A Country</option><option value='US'>United States</option>
                  <option value='CA'>Canada</option>
                  <option value='AF'>Afghanistan</option>
                  <option value='AL'>Albania</option>
                  <option value='DZ'>Algeria</option>
                  <option value='AS'>American Samoa</option>
                  <option value='AD'>Andorra</option>
                  <option value='AO'>Angola</option>
                  <option value='AI'>Anguilla</option>
                  <option value='AQ'>Antarctica</option>
                  <option value='AG'>Antigua And Barbuda</option>
                  <option value='AR'>Argentina</option>
                  <option value='AM'>Armenia</option>
                  <option value='AW'>Aruba</option>
                  <option value='AU'>Australia</option>
                  <option value='AT'>Austria</option>
                  <option value='AZ'>Azerbaijan</option>
                  <option value='BS'>Bahamas</option>
                  <option value='BH'>Bahrain</option>
                  <option value='BD'>Bangladesh</option>
                  <option value='BB'>Barbados</option>
                  <option value='BY'>Belarus</option>
                  <option value='BE'>Belgium</option>
                  <option value='BZ'>Belize</option>
                  <option value='BJ'>Benin</option>
                  <option value='BM'>Bermuda</option>
                  <option value='BT'>Bhutan</option>
                  <option value='BO'>Bolivia</option>
                  <option value='BA'>Bosnia And Herzegovina</option>
                  <option value='BW'>Botswana</option>
                  <option value='BV'>Bouvet Island</option>
                  <option value='BR'>Brazil</option>
                  <option value='IO'>British Indian Ocean Territory</option>
                  <option value='BN'>Brunei</option>
                  <option value='BG'>Bulgaria</option>
                  <option value='BF'>Burkina Faso</option>
                  <option value='BI'>Burundi</option>
                  <option value='KH'>Cambodia</option>
                  <option value='CM'>Cameroon</option>
                  <option value='CV'>Cape Verde</option>
                  <option value='KY'>Cayman Islands</option>
                  <option value='CF'>Central African Republic</option>
                  <option value='TD'>Chad</option>
                  <option value='CL'>Chile</option>
                  <option value='CN'>China</option>
                  <option value='CX'>Christmas Island</option>
                  <option value='CC'>Cocos (Keeling) Islands</option>
                  <option value='CO'>Columbia</option>
                  <option value='KM'>Comoros</option>
                  <option value='CG'>Congo</option>
                  <option value='CK'>Cook Islands</option>
                  <option value='CR'>Costa Rica</option>
                  <option value='CI'>Cote D'Ivorie (Ivory Coast)</option>
                  <option value='HR'>Croatia (Hrvatska)</option>
                  <option value='CU'>Cuba</option>
                  <option value='CY'>Cyprus</option>
                  <option value='CZ'>Czech Republic</option>
                  <option value='CD'>Democratic Republic Of Congo (Zaire)</option>
                  <option value='DK'>Denmark</option>
                  <option value='DJ'>Djibouti</option>
                  <option value='DM'>Dominica</option>
                  <option value='DO'>Dominican Republic</option>
                  <option value='TP'>East Timor</option>
                  <option value='EC'>Ecuador</option>
                  <option value='EG'>Egypt</option>
                  <option value='SV'>El Salvador</option>
                  <option value='GQ'>Equatorial Guinea</option>
                  <option value='ER'>Eritrea</option>
                  <option value='EE'>Estonia</option>
                  <option value='ET'>Ethiopia</option>
                  <option value='FK'>Falkland Islands (Malvinas)</option>
                  <option value='FO'>Faroe Islands</option>
                  <option value='FJ'>Fiji</option>
                  <option value='FI'>Finland</option>
                  <option value='FR'>France</option>
                  <option value='FX'>France, Metropolitan</option>
                  <option value='GF'>French Guinea</option>
                  <option value='PF'>French Polynesia</option>
                  <option value='TF'>French Southern Territories</option>
                  <option value='GA'>Gabon</option>
                  <option value='GM'>Gambia</option>
                  <option value='GE'>Georgia</option>
                  <option value='DE'>Germany</option>
                  <option value='GH'>Ghana</option>
                  <option value='GI'>Gibraltar</option>
                  <option value='GR'>Greece</option>
                  <option value='GL'>Greenland</option>
                  <option value='GD'>Grenada</option>
                  <option value='GP'>Guadeloupe</option>
                  <option value='GU'>Guam</option>
                  <option value='GT'>Guatemala</option>
                  <option value='GN'>Guinea</option>
                  <option value='GW'>Guinea-Bissau</option>
                  <option value='GY'>Guyana</option>
                  <option value='HT'>Haiti</option>
                  <option value='HM'>Heard And McDonald Islands</option>
                  <option value='HN'>Honduras</option>
                  <option value='HK'>Hong Kong</option>
                  <option value='HU'>Hungary</option>
                  <option value='IS'>Iceland</option>
                  <option value='IN'>India</option>
                  <option value='ID'>Indonesia</option>
                  <option value='IR'>Iran</option>
                  <option value='IQ'>Iraq</option>
                  <option value='IE'>Ireland</option>
                  <option value='IL'>Israel</option>
                  <option value='IT'>Italy</option>
                  <option value='JM'>Jamaica</option>
                  <option value='JP'>Japan</option>
                  <option value='JO'>Jordan</option>
                  <option value='KZ'>Kazakhstan</option>
                  <option value='KE'>Kenya</option>
                  <option value='KI'>Kiribati</option>
                  <option value='KW'>Kuwait</option>
                  <option value='KG'>Kyrgyzstan</option>
                  <option value='LA'>Laos</option>
                  <option value='LV'>Latvia</option>
                  <option value='LB'>Lebanon</option>
                  <option value='LS'>Lesotho</option>
                  <option value='LR'>Liberia</option>
                  <option value='LY'>Libya</option>
                  <option value='LI'>Liechtenstein</option>
                  <option value='LT'>Lithuania</option>
                  <option value='LU'>Luxembourg</option>
                  <option value='MO'>Macau</option>
                  <option value='MK'>Macedonia</option>
                  <option value='MG'>Madagascar</option>
                  <option value='MW'>Malawi</option>
                  <option value='MY'>Malaysia</option>
                  <option value='MV'>Maldives</option>
                  <option value='ML'>Mali</option>
                  <option value='MT'>Malta</option>
                  <option value='MH'>Marshall Islands</option>
                  <option value='MQ'>Martinique</option>
                  <option value='MR'>Mauritania</option>
                  <option value='MU'>Mauritius</option>
                  <option value='YT'>Mayotte</option>
                  <option value='MX'>Mexico</option>
                  <option value='FM'>Micronesia</option>
                  <option value='MD'>Moldova</option>
                  <option value='MC'>Monaco</option>
                  <option value='MN'>Mongolia</option>
                  <option value='MS'>Montserrat</option>
                  <option value='MA'>Morocco</option>
                  <option value='MZ'>Mozambique</option>
                  <option value='MM'>Myanmar (Burma)</option>
                  <option value='NA'>Namibia</option>
                  <option value='NR'>Nauru</option>
                  <option value='NP'>Nepal</option>
                  <option value='NL'>Netherlands</option>
                  <option value='AN'>Netherlands Antilles</option>
                  <option value='NC'>New Caledonia</option>
                  <option value='NZ'>New Zealand</option>
                  <option value='NI'>Nicaragua</option>
                  <option value='NE'>Niger</option>
                  <option value='NG'>Nigeria</option>
                  <option value='NU'>Niue</option>
                  <option value='NF'>Norfolk Island</option>
                  <option value='KP'>North Korea</option>
                  <option value='MP'>Northern Mariana Islands</option>
                  <option value='NO'>Norway</option>
                  <option value='OM'>Oman</option>
                  <option value='PK'>Pakistan</option>
                  <option value='PW'>Palau</option>
                  <option value='PA'>Panama</option>
                  <option value='PG'>Papua New Guinea</option>
                  <option value='PY'>Paraguay</option>
                  <option value='PE'>Peru</option>
                  <option value='PH'>Philippines</option>
                  <option value='PN'>Pitcairn</option>
                  <option value='PL'>Poland</option>
                  <option value='PT'>Portugal</option>
                  <option value='PR'>Puerto Rico</option>
                  <option value='QA'>Qatar</option>
                  <option value='RE'>Reunion</option>
                  <option value='RO'>Romania</option>
                  <option value='RU'>Russia</option>
                  <option value='RW'>Rwanda</option>
                  <option value='SH'>Saint Helena</option>
                  <option value='KN'>Saint Kitts And Nevis</option>
                  <option value='LC'>Saint Lucia</option>
                  <option value='PM'>Saint Pierre And Miquelon</option>
                  <option value='VC'>Saint Vincent And The Grenadines</option>
                  <option value='SM'>San Marino</option>
                  <option value='ST'>Sao Tome And Principe</option>
                  <option value='SA'>Saudi Arabia</option>
                  <option value='SN'>Senegal</option>
                  <option value='SC'>Seychelles</option>
                  <option value='SL'>Sierra Leone</option>
                  <option value='SG'>Singapore</option>
                  <option value='SK'>Slovak Republic</option>
                  <option value='SI'>Slovenia</option>
                  <option value='SB'>Solomon Islands</option>
                  <option value='SO'>Somalia</option>
                  <option value='ZA'>South Africa</option>
                  <option value='GS'>South Georgia And South Sandwich Islands</option>
                  <option value='KR'>South Korea</option>
                  <option value='ES'>Spain</option>
                  <option value='LK'>Sri Lanka</option>
                  <option value='SD'>Sudan</option>
                  <option value='SR'>Suriname</option>
                  <option value='SJ'>Svalbard And Jan Mayen</option>
                  <option value='SZ'>Swaziland</option>
                  <option value='SE'>Sweden</option>
                  <option value='CH'>Switzerland</option>
                  <option value='SY'>Syria</option>
                  <option value='TW'>Taiwan</option>
                  <option value='TJ'>Tajikistan</option>
                  <option value='TZ'>Tanzania</option>
                  <option value='TH'>Thailand</option>
                  <option value='TG'>Togo</option>
                  <option value='TK'>Tokelau</option>
                  <option value='TO'>Tonga</option>
                  <option value='TT'>Trinidad And Tobago</option>
                  <option value='TN'>Tunisia</option>
                  <option value='TR'>Turkey</option>
                  <option value='TM'>Turkmenistan</option>
                  <option value='TC'>Turks And Caicos Islands</option>
                  <option value='TV'>Tuvalu</option>
                  <option value='UG'>Uganda</option>
                  <option value='UA'>Ukraine</option>
                  <option value='AE'>United Arab Emirates</option>
                  <option value='UK'>United Kingdom</option>
                  <option value='UM'>United States Minor Outlying Islands</option>
                  <option value='UY'>Uruguay</option>
                  <option value='UZ'>Uzbekistan</option>
                  <option value='VU'>Vanuatu</option>
                  <option value='VA'>Vatican City (Holy See)</option>
                  <option value='VE'>Venezuela</option>
                  <option value='VN'>Vietnam</option>
                  <option value='VG'>Virgin Islands (British)</option>
                  <option value='VI'>Virgin Islands (US)</option>
                  <option value='WF'>Wallis And Futuna Islands</option>
                  <option value='EH'>Western Sahara</option>
                  <option value='WS'>Western Samoa</option>
                  <option value='YE'>Yemen</option>
                  <option value='YU'>Yugoslavia</option>
                  <option value='ZM'>Zambia</option>
                  <option value='ZW'>Zimbabwe</option>
                  </select>                        
                   <!--
                   -->

                    </div>
                    <div class='form-group'>
                      <label for='phone_number'>Phone Number</label>
                      <input type='text' class='form-control' id='phone_number' name='phone_number' placeholder='Phone Number' required>
                    </div>
                    <div class='form-group'>
                      <label for='date_birth'>Date of Birth</label>
                       <table>
                              <tr>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_month' required>
                                  <option value=''>Month</option>
                                  <option value='01'>01 - January</option>
                                  <option value='02'>02 - February</option>
                                  <option value='03'>03 - March</option>
                                  <option value='04'>04 - April</option>
                                  <option value='05'>05 - May</option>
                                  <option value='06'>06 - June</option>
                                  <option value='07'>07 - July</option>
                                  <option value='08'>08 - August</option>
                                  <option value='09'>09 - September</option>
                                  <option value='10'>10 - October</option>
                                  <option value='11'>11 - November</option>
                                  <option value='12'>12 - December</option>
                                </select>
                              </td>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_day' required>
                                
                                  <option value=''>Day</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option>
                                </select>
                              </td>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_year' required>
                                
                                  <option value=''>Year</option><option value='2011'>2011</option><option value='2010'>2010</option><option value='2009'>2009</option><option value='2008'>2008</option><option value='2007'>2007</option><option value='2006'>2006</option><option value='2005'>2005</option><option value='2004'>2004</option><option value='2003'>2003</option><option value='2002'>2002</option><option value='2001'>2001</option><option value='2000'>2000</option><option value='1999'>1999</option><option value='1998'>1998</option><option value='1997'>1997</option><option value='1996'>1996</option><option value='1995'>1995</option><option value='1994'>1994</option><option value='1993'>1993</option><option value='1992'>1992</option><option value='1991'>1991</option><option value='1990'>1990</option><option value='1989'>1989</option><option value='1988'>1988</option><option value='1987'>1987</option><option value='1986'>1986</option><option value='1985'>1985</option><option value='1984'>1984</option><option value='1983'>1983</option><option value='1982'>1982</option><option value='1981'>1981</option><option value='1980'>1980</option><option value='1979'>1979</option><option value='1978'>1978</option><option value='1977'>1977</option><option value='1976'>1976</option><option value='1975'>1975</option><option value='1974'>1974</option><option value='1973'>1973</option><option value='1972'>1972</option><option value='1971'>1971</option><option value='1970'>1970</option><option value='1969'>1969</option><option value='1968'>1968</option><option value='1967'>1967</option><option value='1966'>1966</option><option value='1965'>1965</option><option value='1964'>1964</option><option value='1963'>1963</option><option value='1962'>1962</option><option value='1961'>1961</option><option value='1960'>1960</option><option value='1959'>1959</option><option value='1958'>1958</option><option value='1957'>1957</option><option value='1956'>1956</option><option value='1955'>1955</option><option value='1954'>1954</option><option value='1953'>1953</option><option value='1952'>1952</option><option value='1951'>1951</option><option value='1950'>1950</option><option value='1949'>1949</option><option value='1948'>1948</option><option value='1947'>1947</option><option value='1946'>1946</option><option value='1945'>1945</option><option value='1944'>1944</option><option value='1943'>1943</option><option value='1942'>1942</option><option value='1941'>1941</option><option value='1940'>1940</option><option value='1939'>1939</option><option value='1938'>1938</option><option value='1937'>1937</option><option value='1936'>1936</option><option value='1935'>1935</option><option value='1934'>1934</option><option value='1933'>1933</option><option value='1932'>1932</option><option value='1931'>1931</option><option value='1930'>1930</option><option value='1929'>1929</option><option value='1928'>1928</option><option value='1927'>1927</option><option value='1926'>1926</option><option value='1925'>1925</option><option value='1924'>1924</option><option value='1923'>1923</option><option value='1922'>1922</option><option value='1921'>1921</option><option value='1920'>1920</option><option value='1919'>1919</option><option value='1918'>1918</option><option value='1917'>1917</option></select>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <div class='form-group'>
                            <label  for='firstname'>
                              <b>Gender:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='gender' value='Male' > Male &nbsp; &nbsp; 
                                <input type='radio' name='gender' value='Female' > Female
                                <input type='radio' name='gender' value='Other' > Other
                              <br/>(NOTE to applicant: We do not discriminate)
                            </div>
                          </div>
  <div class="radio">
  <label>
    <input type="radio" name="admin_priv" id="optionsRadios1" value="Reader" checked>
    Reader
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="admin_priv" id="optionsRadios2" value="Author" checked>
    Author
  </label>
</div>

  <div class="form-group">
    <label for="imagen">Imagen Input</label>
    <input type="file"class="form-control" name="userfile" value="fichero"/>
  </div>
  
   <div class="g-recaptcha" data-sitekey="6Ldh8BYTAAAAAJiD7_oqxFMLByX6unUoyRtxKZcz"></div>
    <div class="form-group">
        <div class="col-md-3 col-md-offset-3">
            <div id="messages"></div>
        </div>
    </div>
    <span class='note'>
                        <p>
                            <label><input type='checkbox' name='agree_terms' value='y'  /> I agree to the JaysonLynn.Net Inc terms of agreement</label> (click here for details on the agreement)
                        </p>
                        <p>
                            <label><input type='checkbox' name='accept_contractual' value='y'  /> I acknowledge that if accepted as a Reader for any JaysonLynn.Net Inc. website- that I am being listed as an Independent Contractor</label>
                        </p>
                        <p>
                            <label><input type='checkbox' name='accept_responsibility' value='y'  /> I acknowledge that if accepted as a Reader to Read on or for any JaysonLynn.Net Inc website- that I am RESPONSIBLE FOR PAYING MY OWN TAXES FOR ALL MONIES EARNED BY ME FOR FURNISHING READINGS TO JAYSONLYNN.NET CLIENTELE</label>
                        </p>
                        By clicking the 'Submit Now' button you are attesting to the accuracy and truthfulness of the information you have entered. Any inaccurate information or false statements will result in immediate termination if you are hired.
                    </span >
            <div style="text-align: center; width:100%; padding-top: 20px;"><button  name="button" id="button" type="submit">Signup</button></div>

</form>


<script type="text/javascript">$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            exp_years: {
                validators: {
                    notEmpty: {
                        message: 'The experience years is required and cannot be empty'
                    }
                }
            },
            b_year: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_day: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_month: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: 'The phone number is required and cannot be empty'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>


   </div>
    
    
    
    </div>
    

    </div>
    
    
    
    <!-- SIDE BAR-->
 <?php  /*include('formula/side_menu.php');*/ ?>

<!-- SIDE BAR-->
 
    </div>
    </div>
</div> <!-- FOOTER -->


  <?php include('footer.php');?>

</body>
</html>


