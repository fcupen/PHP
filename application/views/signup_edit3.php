<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>

<style type="text/css">
@media screen and (max-width:530px){
.respon{
  transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
}</style>

</head>
<body>
  <?php include('menu.php');?>
  <!-- MENU -->

<div id="slide">  
<div id="author">
<div id="link"><div id="text"><a href="https://www.astral-foundations.com/"><font style="color:white;"> Home</font></a> > <strong>Sign Up</strong></div></div>
    
    <div id="alg_profile" >
  
    
    <div id="profile" >
  
<div>
<?php if(isset($error)){/*echo $error;*/}?>
<?php foreach ($success as $item => $value) {
  echo '<li>'.$item.' - '.$value.'</li>';
}?>
<form data-toggle="validator" role="form"  id="contactForm" action="https://www.astral-foundations.com/users/edit_info_prof2/" method="POST" enctype="multipart/form-data"> 
  <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Edit Personal Information</div>
  
  <div class='form-group'>
    <label for='first_name'>First Name</label>
    <input type='text' class='form-control' id='first_name' name='first_name' value='<?=$information->first_name?>' required>
  </div>
  <div class='form-group'>
    <label for='last_name'>Last Name</label>
    <input type='text' class='form-control' id='last_name' name='last_name'  value='<?=$information->last_name?>'  required>
  </div>
  <div class='form-group'>
    <label for='address_1'>Address Line 1</label>
    <input type='text' class='form-control' id='address_1' name='address_1' value='<?=$information->address_1?>' >
  </div>
  <div class='form-group'>
    <label for='address_2'>Address Line 2</label>
    <input type='text' class='form-control' id='address_2' name='address_2' value='<?=$information->address_2?>' >
  </div>
  <div class='form-group'>
    <label for='city_town'>City/Town</label>
    <input type='text' class='form-control' id='city_town' name='city_town' value='<?=$information->city_town?>' >
  </div>
  <div class='form-group'>
    <label for='state_province'>State/Province</label>
    <input type='text' class='form-control' id='state_province' name='state_province' value='<?=$information->state_province?>' >
  </div>
  <div class='form-group'>
    <label for='zip_postal'>Zip/Postal</label>
    <input type='text' class='form-control' id='zip_postal' name='zip_postal' value='<?=$information->zip_postal?>' >
  </div>
                    <div class='form-group'>
                    <div class='respon'>
                      <label for='country'>Country</label>
                     <select style='width:auto;' name='country' class='form-control'  required><option  value='<?=$information->country?>' ><?=$information->country?> </option>
                     <option value='US'>United States</option>
                  <option value='CA' >Canada</option>
                  <option value='AF'>Afghanistan</option>
                  <option value='AL'>Albania</option>
                  <option value='DZ'>Algeria</option>
                  <option value='AS'>American Samoa</option>
                  <option value='AD'>Andorra</option>
                  <option value='AO'>Angola</option>
                  <option value='AI'>Anguilla</option>
                  <option value='AQ'>Antarctica</option>
                  <option value='AG'>Antigua And Barbuda</option>
                  <option value='AR'>Argentina</option>
                  <option value='AM'>Armenia</option>
                  <option value='AW'>Aruba</option>
                  <option value='AU'>Australia</option>
                  <option value='AT'>Austria</option>
                  <option value='AZ'>Azerbaijan</option>
                  <option value='BS'>Bahamas</option>
                  <option value='BH'>Bahrain</option>
                  <option value='BD'>Bangladesh</option>
                  <option value='BB'>Barbados</option>
                  <option value='BY'>Belarus</option>
                  <option value='BE'>Belgium</option>
                  <option value='BZ'>Belize</option>
                  <option value='BJ'>Benin</option>
                  <option value='BM'>Bermuda</option>
                  <option value='BT'>Bhutan</option>
                  <option value='BO'>Bolivia</option>
                  <option value='BA'>Bosnia And Herzegovina</option>
                  <option value='BW'>Botswana</option>
                  <option value='BV'>Bouvet Island</option>
                  <option value='BR'>Brazil</option>
                  <option value='IO'>British Indian Ocean Territory</option>
                  <option value='BN'>Brunei</option>
                  <option value='BG'>Bulgaria</option>
                  <option value='BF'>Burkina Faso</option>
                  <option value='BI'>Burundi</option>
                  <option value='KH'>Cambodia</option>
                  <option value='CM'>Cameroon</option>
                  <option value='CV'>Cape Verde</option>
                  <option value='KY'>Cayman Islands</option>
                  <option value='CF'>Central African Republic</option>
                  <option value='TD'>Chad</option>
                  <option value='CL'>Chile</option>
                  <option value='CN'>China</option>
                  <option value='CX'>Christmas Island</option>
                  <option value='CC'>Cocos (Keeling) Islands</option>
                  <option value='CO'>Columbia</option>
                  <option value='KM'>Comoros</option>
                  <option value='CG'>Congo</option>
                  <option value='CK'>Cook Islands</option>
                  <option value='CR'>Costa Rica</option>
                  <option value='CI'>Cote D'Ivorie (Ivory Coast)</option>
                  <option value='HR'>Croatia (Hrvatska)</option>
                  <option value='CU'>Cuba</option>
                  <option value='CY'>Cyprus</option>
                  <option value='CZ'>Czech Republic</option>
                  <option value='CD'>Democratic Republic Of Congo (Zaire)</option>
                  <option value='DK'>Denmark</option>
                  <option value='DJ'>Djibouti</option>
                  <option value='DM'>Dominica</option>
                  <option value='DO'>Dominican Republic</option>
                  <option value='TP'>East Timor</option>
                  <option value='EC'>Ecuador</option>
                  <option value='EG'>Egypt</option>
                  <option value='SV'>El Salvador</option>
                  <option value='GQ'>Equatorial Guinea</option>
                  <option value='ER'>Eritrea</option>
                  <option value='EE'>Estonia</option>
                  <option value='ET'>Ethiopia</option>
                  <option value='FK'>Falkland Islands (Malvinas)</option>
                  <option value='FO'>Faroe Islands</option>
                  <option value='FJ'>Fiji</option>
                  <option value='FI'>Finland</option>
                  <option value='FR'>France</option>
                  <option value='FX'>France, Metropolitan</option>
                  <option value='GF'>French Guinea</option>
                  <option value='PF'>French Polynesia</option>
                  <option value='TF'>French Southern Territories</option>
                  <option value='GA'>Gabon</option>
                  <option value='GM'>Gambia</option>
                  <option value='GE'>Georgia</option>
                  <option value='DE'>Germany</option>
                  <option value='GH'>Ghana</option>
                  <option value='GI'>Gibraltar</option>
                  <option value='GR'>Greece</option>
                  <option value='GL'>Greenland</option>
                  <option value='GD'>Grenada</option>
                  <option value='GP'>Guadeloupe</option>
                  <option value='GU'>Guam</option>
                  <option value='GT'>Guatemala</option>
                  <option value='GN'>Guinea</option>
                  <option value='GW'>Guinea-Bissau</option>
                  <option value='GY'>Guyana</option>
                  <option value='HT'>Haiti</option>
                  <option value='HM'>Heard And McDonald Islands</option>
                  <option value='HN'>Honduras</option>
                  <option value='HK'>Hong Kong</option>
                  <option value='HU'>Hungary</option>
                  <option value='IS'>Iceland</option>
                  <option value='IN'>India</option>
                  <option value='ID'>Indonesia</option>
                  <option value='IR'>Iran</option>
                  <option value='IQ'>Iraq</option>
                  <option value='IE'>Ireland</option>
                  <option value='IL'>Israel</option>
                  <option value='IT'>Italy</option>
                  <option value='JM'>Jamaica</option>
                  <option value='JP'>Japan</option>
                  <option value='JO'>Jordan</option>
                  <option value='KZ'>Kazakhstan</option>
                  <option value='KE'>Kenya</option>
                  <option value='KI'>Kiribati</option>
                  <option value='KW'>Kuwait</option>
                  <option value='KG'>Kyrgyzstan</option>
                  <option value='LA'>Laos</option>
                  <option value='LV'>Latvia</option>
                  <option value='LB'>Lebanon</option>
                  <option value='LS'>Lesotho</option>
                  <option value='LR'>Liberia</option>
                  <option value='LY'>Libya</option>
                  <option value='LI'>Liechtenstein</option>
                  <option value='LT'>Lithuania</option>
                  <option value='LU'>Luxembourg</option>
                  <option value='MO'>Macau</option>
                  <option value='MK'>Macedonia</option>
                  <option value='MG'>Madagascar</option>
                  <option value='MW'>Malawi</option>
                  <option value='MY'>Malaysia</option>
                  <option value='MV'>Maldives</option>
                  <option value='ML'>Mali</option>
                  <option value='MT'>Malta</option>
                  <option value='MH'>Marshall Islands</option>
                  <option value='MQ'>Martinique</option>
                  <option value='MR'>Mauritania</option>
                  <option value='MU'>Mauritius</option>
                  <option value='YT'>Mayotte</option>
                  <option value='MX'>Mexico</option>
                  <option value='FM'>Micronesia</option>
                  <option value='MD'>Moldova</option>
                  <option value='MC'>Monaco</option>
                  <option value='MN'>Mongolia</option>
                  <option value='MS'>Montserrat</option>
                  <option value='MA'>Morocco</option>
                  <option value='MZ'>Mozambique</option>
                  <option value='MM'>Myanmar (Burma)</option>
                  <option value='NA'>Namibia</option>
                  <option value='NR'>Nauru</option>
                  <option value='NP'>Nepal</option>
                  <option value='NL'>Netherlands</option>
                  <option value='AN'>Netherlands Antilles</option>
                  <option value='NC'>New Caledonia</option>
                  <option value='NZ'>New Zealand</option>
                  <option value='NI'>Nicaragua</option>
                  <option value='NE'>Niger</option>
                  <option value='NG'>Nigeria</option>
                  <option value='NU'>Niue</option>
                  <option value='NF'>Norfolk Island</option>
                  <option value='KP'>North Korea</option>
                  <option value='MP'>Northern Mariana Islands</option>
                  <option value='NO'>Norway</option>
                  <option value='OM'>Oman</option>
                  <option value='PK'>Pakistan</option>
                  <option value='PW'>Palau</option>
                  <option value='PA'>Panama</option>
                  <option value='PG'>Papua New Guinea</option>
                  <option value='PY'>Paraguay</option>
                  <option value='PE'>Peru</option>
                  <option value='PH'>Philippines</option>
                  <option value='PN'>Pitcairn</option>
                  <option value='PL'>Poland</option>
                  <option value='PT'>Portugal</option>
                  <option value='PR'>Puerto Rico</option>
                  <option value='QA'>Qatar</option>
                  <option value='RE'>Reunion</option>
                  <option value='RO'>Romania</option>
                  <option value='RU'>Russia</option>
                  <option value='RW'>Rwanda</option>
                  <option value='SH'>Saint Helena</option>
                  <option value='KN'>Saint Kitts And Nevis</option>
                  <option value='LC'>Saint Lucia</option>
                  <option value='PM'>Saint Pierre And Miquelon</option>
                  <option value='VC'>Saint Vincent And The Grenadines</option>
                  <option value='SM'>San Marino</option>
                  <option value='ST'>Sao Tome And Principe</option>
                  <option value='SA'>Saudi Arabia</option>
                  <option value='SN'>Senegal</option>
                  <option value='SC'>Seychelles</option>
                  <option value='SL'>Sierra Leone</option>
                  <option value='SG'>Singapore</option>
                  <option value='SK'>Slovak Republic</option>
                  <option value='SI'>Slovenia</option>
                  <option value='SB'>Solomon Islands</option>
                  <option value='SO'>Somalia</option>
                  <option value='ZA'>South Africa</option>
                  <option value='GS'>South Georgia And South Sandwich Islands</option>
                  <option value='KR'>South Korea</option>
                  <option value='ES'>Spain</option>
                  <option value='LK'>Sri Lanka</option>
                  <option value='SD'>Sudan</option>
                  <option value='SR'>Suriname</option>
                  <option value='SJ'>Svalbard And Jan Mayen</option>
                  <option value='SZ'>Swaziland</option>
                  <option value='SE'>Sweden</option>
                  <option value='CH'>Switzerland</option>
                  <option value='SY'>Syria</option>
                  <option value='TW'>Taiwan</option>
                  <option value='TJ'>Tajikistan</option>
                  <option value='TZ'>Tanzania</option>
                  <option value='TH'>Thailand</option>
                  <option value='TG'>Togo</option>
                  <option value='TK'>Tokelau</option>
                  <option value='TO'>Tonga</option>
                  <option value='TT'>Trinidad And Tobago</option>
                  <option value='TN'>Tunisia</option>
                  <option value='TR'>Turkey</option>
                  <option value='TM'>Turkmenistan</option>
                  <option value='TC'>Turks And Caicos Islands</option>
                  <option value='TV'>Tuvalu</option>
                  <option value='UG'>Uganda</option>
                  <option value='UA'>Ukraine</option>
                  <option value='AE'>United Arab Emirates</option>
                  <option value='UK'>United Kingdom</option>
                  <option value='UM'>United States Minor Outlying Islands</option>
                  <option value='UY'>Uruguay</option>
                  <option value='UZ'>Uzbekistan</option>
                  <option value='VU'>Vanuatu</option>
                  <option value='VA'>Vatican City (Holy See)</option>
                  <option value='VE'>Venezuela</option>
                  <option value='VN'>Vietnam</option>
                  <option value='VG'>Virgin Islands (British)</option>
                  <option value='VI'>Virgin Islands (US)</option>
                  <option value='WF'>Wallis And Futuna Islands</option>
                  <option value='EH'>Western Sahara</option>
                  <option value='WS'>Western Samoa</option>
                  <option value='YE'>Yemen</option>
                  <option value='YU'>Yugoslavia</option>
                  <option value='ZM'>Zambia</option>
                  <option value='ZW'>Zimbabwe</option>
                  </select>                        
                   <!--
                   -->
</div>
                    </div>
                    <div class='form-group'>
                      <label for='phone_number'>Phone Number</label>
                      <input type='text' class='form-control' id='phone_number' name='phone_number'  value='<?=$information->phone_number?>'  required>
                    </div>
                    <div class='form-group'>
                      <label for='date_birth'>Date of Birth</label>
                       <table>
                              <tr>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_month' required>
                                  <option  value='<?=$information->b_month?>' ><?php if($information->b_month =='1'){echo '01 - January';}?><?php if($information->b_month =='2'){echo '02 - February';}?><?php if($information->b_month =='3'){echo '03 - March';}?><?php if($information->b_month =='4'){echo '04 - April';}?><?php if($information->b_month =='5'){echo '05 - May';}?><?php if($information->b_month =='6'){echo '06 - June';}?><?php if($information->b_month =='7'){echo '07 - July';}?><?php if($information->b_month =='8'){echo '08 - August';}?><?php if($information->b_month =='9'){echo '09 - September';}?><?php if($information->b_month =='10'){echo '10 - October';}?><?php if($information->b_month =='11'){echo '11 - November';}?><?php if($information->b_month =='12'){echo '12 - December';}?> </option>
                                  <option value='01'>01 - January</option>
                                  <option value='02'>02 - February</option>
                                  <option value='03'>03 - March</option>
                                  <option value='04'>04 - April</option>
                                  <option value='05'>05 - May</option>
                                  <option value='06'>06 - June</option>
                                  <option value='07'>07 - July</option>
                                  <option value='08'>08 - August</option>
                                  <option value='09'>09 - September</option>
                                  <option value='10'>10 - October</option>
                                  <option value='11'>11 - November</option>
                                  <option value='12'>12 - December</option>
                                </select>
                              </td>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_day' required>
                                
                                  <option  value='<?=$information->b_day?>' > <?=$information->b_day?> </option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option>
                                </select>
                              </td>
                              <td>
                                <select style='width:auto;' class='form-control' name='b_year' required>
                                
                                  <option  value='<?=$information->b_year?>' > <?=$information->b_year?></option><option value='2011'>2011</option><option value='2010'>2010</option><option value='2009'>2009</option><option value='2008'>2008</option><option value='2007'>2007</option><option value='2006'>2006</option><option value='2005'>2005</option><option value='2004'>2004</option><option value='2003'>2003</option><option value='2002'>2002</option><option value='2001'>2001</option><option value='2000'>2000</option><option value='1999'>1999</option><option value='1998'>1998</option><option value='1997'>1997</option><option value='1996'>1996</option><option value='1995'>1995</option><option value='1994'>1994</option><option value='1993'>1993</option><option value='1992'>1992</option><option value='1991'>1991</option><option value='1990'>1990</option><option value='1989'>1989</option><option value='1988'>1988</option><option value='1987'>1987</option><option value='1986'>1986</option><option value='1985'>1985</option><option value='1984'>1984</option><option value='1983'>1983</option><option value='1982'>1982</option><option value='1981'>1981</option><option value='1980'>1980</option><option value='1979'>1979</option><option value='1978'>1978</option><option value='1977'>1977</option><option value='1976'>1976</option><option value='1975'>1975</option><option value='1974'>1974</option><option value='1973'>1973</option><option value='1972'>1972</option><option value='1971'>1971</option><option value='1970'>1970</option><option value='1969'>1969</option><option value='1968'>1968</option><option value='1967'>1967</option><option value='1966'>1966</option><option value='1965'>1965</option><option value='1964'>1964</option><option value='1963'>1963</option><option value='1962'>1962</option><option value='1961'>1961</option><option value='1960'>1960</option><option value='1959'>1959</option><option value='1958'>1958</option><option value='1957'>1957</option><option value='1956'>1956</option><option value='1955'>1955</option><option value='1954'>1954</option><option value='1953'>1953</option><option value='1952'>1952</option><option value='1951'>1951</option><option value='1950'>1950</option><option value='1949'>1949</option><option value='1948'>1948</option><option value='1947'>1947</option><option value='1946'>1946</option><option value='1945'>1945</option><option value='1944'>1944</option><option value='1943'>1943</option><option value='1942'>1942</option><option value='1941'>1941</option><option value='1940'>1940</option><option value='1939'>1939</option><option value='1938'>1938</option><option value='1937'>1937</option><option value='1936'>1936</option><option value='1935'>1935</option><option value='1934'>1934</option><option value='1933'>1933</option><option value='1932'>1932</option><option value='1931'>1931</option><option value='1930'>1930</option><option value='1929'>1929</option><option value='1928'>1928</option><option value='1927'>1927</option><option value='1926'>1926</option><option value='1925'>1925</option><option value='1924'>1924</option><option value='1923'>1923</option><option value='1922'>1922</option><option value='1921'>1921</option><option value='1920'>1920</option><option value='1919'>1919</option><option value='1918'>1918</option><option value='1917'>1917</option></select>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <div class='form-group'>
                            <label  for='firstname'>
                              <b>Gender:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='gender' value='Male' <?php if($information->gender == 'Male' ){?>checked<?php }; ?>  > Male &nbsp; &nbsp; 
                                <input type='radio' name='gender' value='Female' <?php if($information->gender == 'Female' ){?>checked<?php }; ?>  > Female
                                <input type='radio' name='gender' value='Other' <?php if($information->gender == 'Other' ){?>checked<?php }; ?>  > Other
                              <br/>(NOTE to applicant: We do not discriminate)
                            </div>
                          </div>
                          <br/>
                          <br/>
                          <h1>PLEASE ANSWER THE FOLLOWING AS TRUTHFULLY AS POSSIBLE</h1>
                          
                        <div class='form-group'>
                            <label>Are you a U.S. resident?: (If so, we will ask for it later) </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q1' id='q1' value="n" <?php if($information->q1 == 'n' ){?>checked<?php }; ?>  >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q1' id='q1'  value="y" <?php if($information->q1 == 'y' ){?>checked<?php }; ?>  >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>SSN/SIN (If so, we will ask for it later)  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q2' id='q2' value='n'  <?php if($information->q2 == 'n' ){?>checked<?php }; ?>  >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q2' id='q2' value='y' <?php if($information->q2 == 'y' ){?>checked<?php }; ?>  >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Will you be able to furnish us with your Govt. issued Photo ID (driver's license/passport/med I.D. card etc.)?
                        INSTRUCTIONS FOR SENDING YOUR PHOTO I.D.WILL BE EMAILED TO YOU UPON ACCEPTANCE </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q3' id='q3' value='n' <?php if($information->q3 == 'n' ){?>checked<?php }; ?>  >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q3' id='q3' value='y' <?php if($information->q3 == 'y' ){?>checked<?php }; ?>  >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Do you know any readers or clients on our site(s)? </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q4' id='q4' value='n' <?php if($information->q4 == 'n' ){?>checked<?php }; ?> >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q4' id='q4' value='y' <?php if($information->q4 == 'y' ){?>checked<?php }; ?>  >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Are you related to, or know any other recent applicants to our site(s)?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q5' id='q5' value='n' <?php if($information->q5 == 'n' ){?>checked<?php }; ?>  >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q5' id='q5' value='y' <?php if($information->q5 == 'y' ){?>checked<?php }; ?>  >
                                Yes
                              </label>
                            </div>
                        </div>

<div class='form-group'>
                            <label for='username'>Are you willing to be tested?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q6' id='q6' value='n' <?php if($information->q6 == 'n' ){?>checked<?php }; ?> >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q6' id='q6' value='y' <?php if($information->q6 == 'y' ){?>checked<?php }; ?> >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Are you willing to promote our sites by writing articles and/or blogging?
                        If you have no experience with this- are you willing to learn?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q7' id='q7' value='n' <?php if($information->q7 == 'n' ){?>checked<?php }; ?> >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q7' id='q7' value='y' <?php if($information->q7 == 'y' ){?>checked<?php }; ?> >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                List your experience in Article Writing and/or Blogging:
                            </label>
                            <div >                            
                                <input type='text' name='writing_years' style='width: 15%; display: inline' class='form-control'  value='<?=$information->writing_years?>'  /> years
                                <input type='radio' name='writing_years' value='n' <?php if($information->writing_years == 'n' ){?>checked<?php }; ?>  > No &nbsp;                                
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                Do you have a Skype acct? Skype is required!!!
                                <br/>(It's how the Admin communicates with the Readers)
                            </label>
                            <div>                            
                                <input type='radio' name='has_skype' value='n' <?php if($information->has_skype == 'n' ){?>checked<?php }; ?> > No &nbsp;
                                <input type='radio' name='has_skype' value='y' <?php if($information->has_skype == 'y' ){?>checked<?php }; ?>  > Yes
                                <br/>
                                <input type='text' placeholder='PLEASE ENTER YOUR SKYPE CONTACT ID HERE' name='skype_id' id='skype_id' class='form-control'  value='<?=$information->skype_id?>'  />
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >Do you have a PayPal acct?
                        (Required)  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='has_paypal' id='has_paypal' value='n' <?php if($information->has_paypal == 'n' ){?>checked<?php }; ?>>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='has_paypal' id='has_paypal' value='y' <?php if($information->has_paypal == 'y' ){?>checked<?php }; ?>>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >If you are based in the USA: Do you have a U.S. based checking acct?
                        (you will need one so that we can pay you via Direct Deposit)</label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='based_usa' id='based_usa' value='n'<?php if($information->based_usa == 'n' ){?>checked<?php }; ?>>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='based_usa' id='based_usa' value='y'<?php if($information->based_usa == 'y' ){?>checked<?php }; ?>>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >Are you currently on any govt. disability or subsidy program? </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='disability' id='disability' value='n'<?php if($information->disability == 'n' ){?>checked<?php }; ?>>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='disability' id='disability' value='y'<?php if($information->disability == 'y' ){?>checked<?php }; ?>>
                                Yes
                              </label>
                            </div>
                        </div>

<h1>Your Computer / Internet / Technical Information</h1>

                        <div class='form-group'>
                            <label for='ISP'>Who is your ISP? (Internet Service Provider)</label>
                            <input type='text' class='form-control' id='ISP' name='ISP'  value='<?=$information->ISP?>' >
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>How old is your Computer/Device</b>
                                <br/>(the one you will use for our sites/Readings)
                            </label>
                            <div>                            
                                <input type='text' name='pc_age' style='width:15%; display: inline;' class='form-control'  value='<?=$information->pc_age?>' >/years
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>Operating System</b>
                            </label>
                            <div >          
                                <input type='checkbox' name='device[]'  value='<?=$information->device_mac?>'  > MAC:
                                <input type='text' name='device_mac' value='' class='form-control' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]'  value='<?=$information->device_ipad?>'  > I-Pad:
                                <input type='text' name='device_ipad' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]'  value='<?=$information->device_ios?>'  > iOS:
                                <input type='text' name='device_ios' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]'  value='<?=$information->device_droid?>'  > Droid:
                                <input type='text' name='device_droid' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]'  value='<?=$information->device_other?>'  > Other:
                                <input type='text' name='device_other' class='form-control' value='' style='width: 60%; display: inline' /> <br/><br/>                                
                            </div>
                        </div>

<br/><br/><h1>Your Qualifications</h1>

                          <div class='form-group'>
                            <label>
                                <b>Do you read and write English fluently?:</b>
                                (This website supports primarily the English language. 
                                BUT WE WILL CONSIDER ACCEPTING APPLICANTS THAT CAN ALSO GIVE CLIENTS READINGS IN OTHER LANGUAGES.
                                (Applicants who require translation software will not be considered.)
                            </label>
                            <div >                            
                                <label><input type='radio' name='en_fluent' value='y'<?php if($information->en_fluent == 'y' ){?>checked<?php }; ?>> Yes, I am fluent in English </label><br/>
                                <label><input type='radio' name='en_fluent' value='n'<?php if($information->en_fluent == 'n' ){?>checked<?php }; ?>> No, I am NOT fluent in English</label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Other Languages:</b>
                            </label>
                            <div >                            
                                <input type='text' name='languages' class='form-control'  value='<?=$information->languages?>' >
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>Please rate your grammar and communication skills:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='com_skills' value='1' <?php if($information->com_skills == '1' ){?>checked<?php }; ?> > 1 (Low)
                                <input type='radio' name='com_skills' value='2' <?php if($information->com_skills == '2' ){?>checked<?php }; ?> > 2 (Ok)
                                <input type='radio' name='com_skills' value='3' <?php if($information->com_skills == '3' ){?>checked<?php }; ?> > 3 (Good)
                                <input type='radio' name='com_skills' value='4' <?php if($information->com_skills == '4' ){?>checked<?php }; ?> > 4 (Great)
                                <input type='radio' name='com_skills' value='5' <?php if($information->com_skills == '5' ){?>checked<?php }; ?> > 5 (Excellent)
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Do you type at least 60 words per minute accurately?:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='typing_skills' value='n' <?php if($information->typing_skills == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='typing_skills' value='y' <?php if($information->typing_skills == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Are you able to commit to a minimum of 15 hours per week?:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='is_committed' value='n'  <?php if($information->is_committed == 'n' ){?>checked<?php }; ?>> No
                                <input type='radio' name='is_committed' value='y'  <?php if($information->is_committed == 'y' ){?>checked<?php }; ?>> Yes
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                Are you available to log on to take client readings/calls during Late Hours/“Wee” hours? (from midnight to 3am EST)
                                <br/>
                                NOTE: IT IS REQUIRED THAT YOU AGREE TO DOING “LATE SHIFT READINGS- AT LEAST ONE NIGHT PER WEEK).
                            </label>
                            <div >                            
                                <input type='radio' name='is_wee_available' value='n' <?php if($information->is_wee_available == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='is_wee_available' value='y' <?php if($information->is_wee_available == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                If accepted: Date Available For Starting Work?:
                            </label>
                            <div>                            
                                <input type='text' name='start_date' id='start_date' class='form-control' style='width: 30%'  value='<?=$information->start_date?>'  />                                
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                <b>What days of the week do you prefer working: </b>
                                <br/>(select ALL if you plan on working 7 days a week - consistently)
                            </label>
                            <div>                            
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Mondays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Tuesdays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Wednesdays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Thursdays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Fridays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Saturdays
                                <input type='checkbox' name='date_avail[]'  value='<?=$information->date_avail?>'  > Sundays
                            </div>
                        </div>



                        <div class='form-group'>
                            <label >
                                <b>Areas of Expertise:</b>
                            </label>
                            <div >                            
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Love / Romance / Relationship Advice <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Career / Finance / Money Advice <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Tarot Cart Readings <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Health (WE URGE CAUTION WITH THESE TYPES OF READINGS!) <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Animal Communication <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Any Holistic Therapies <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Angels / Guides <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Channeling / Mediumship <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Dream Interpretation <br />
                                <input type='checkbox' name='expertise[]'  value='<?=$information->expertise?>'  > Past Life Analysis <br />
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                <b>What forms of <i>Divination Tools</i> do you use for your Readings?:</b> 
                                (You must choose at least one- Please list all that are applicable- NOTE: Some of these
                                may be used on your profile for our sites- if accepted)
                            </label>
                            <div>                            
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Numerology <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Astrology / Horoscopes <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Palm Reading <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Runes <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Cartomancy <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Tarot <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Pendulum <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Tea Leaves <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Crystals <br />
                                <input type='checkbox' name='divine_practices[]'  value='<?=$information->divine_practices?>'  > Other 
                                <input type='text' name='divine_other' value='' class='form-control' style='width: 60%'  value='<?=$information->first_name?>'  />
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                Do you consider yourself:
                                <br/>Clairvoyant?
                            </label>
                            <div>                            
                                <input type='radio' name='is_clairvoyant' value='n' <?php if($information->is_clairvoyant == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='is_clairvoyant' value='y' <?php if($information->is_clairvoyant == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >                        
                                Empathic?
                            </label>
                            <div >                            
                                <input type='radio' name='is_empathic' value='n' <?php if($information->is_empathic == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='is_empathic' value='y' <?php if($information->is_empathic == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >                        
                                Clairsentient?
                            </label>
                            <div >                            
                                <input type='radio' name='is_clairsentient' value='n' <?php if($information->is_clairsentient == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='is_clairsentient' value='y' <?php if($information->is_clairsentient == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>                        
                                Other?
                            </label>
                            <div>                            
                                <input type='text' name='other_self'  value='<?=$information->other_self?>'  class='form-control' />
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                What method of delivering Readings do you prefer to use (select yes to all that apply to you):
                                <br/>Email:
                            </label>
                            <div >                            
                                <input type='radio' name='method_email' value='n' <?php if($information->method_email == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_email' value='y' <?php if($information->method_email == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Chat:
                            </label>
                            <div >                            
                                <input type='radio' name='method_chat' value='n' <?php if($information->method_chat == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_chat' value='y' <?php if($information->method_chat == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Phone:
                            </label>
                            <div>                            
                                <input type='radio' name='method_phone' value='n' <?php if($information->method_phone == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_phone' value='y' <?php if($information->method_phone == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Video Chat:
                            </label>
                            <div>                            
                                <input type='radio' name='method_vidchat' value='n' <?php if($information->method_vidchat == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_vidchat' value='y' <?php if($information->method_vidchat == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                SMS Text:
                            </label>
                            <div>                            
                                <input type='radio' name='method_sms' value='n' <?php if($information->method_sms == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_sms' value='y' <?php if($information->method_sms == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Private In Person:
                            </label>
                            <div>                            
                                <input type='radio' name='method_private_person' value='n' <?php if($information->method_private_person == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_private_person' value='y' <?php if($information->method_private_person == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Radio:
                            </label>
                            <div >                            
                                <input type='radio' name='method_radio' value='n' <?php if($information->method_radio == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_radio' value='y' <?php if($information->method_radio == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                T.V:
                            </label>
                            <div>                            
                                <input type='radio' name='method_tv' value='n'<?php if($information->method_tv == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_tv' value='y'<?php if($information->method_tv == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Public Venue:
                            </label>
                            <div>                            
                                <input type='radio' name='method_public_venue'  value='n' <?php if($information->method_public_venue == 'n' ){?>checked<?php }; ?> > No
                                <input type='radio' name='method_public_venue'  value='y' <?php if($information->method_public_venue == 'y' ){?>checked<?php }; ?> > Yes
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                <b>List All Your <i>NON</i> Divination Skills (in particular as related to the Internet):</b>
                            </label>
                            <div>
                                <label><input type='radio' name='skills' value='n' <?php if($information->skills == 'n' ){?>checked<?php }; ?> > N/A</label>
                                <textarea name='skills' placeholder='Type your NON Divination Skills here' class='form-control' style='height: 150px;'> <?=$information->skills?> </textarea>
                            </div>
                        </div>
                        <br /><br /><h1>Your Experience</h1>

                        <div class='form-group'>
                            <label >
                                <b>*Years of Reading Experience:</b>
                            </label>
                            <div >                            
                                <input type='text' name='exp_years' class='form-control' style='width:15%; display: inline'  value='<?=$information->exp_years?>' value=''> years
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                <b>Companies Worked For:</b> PLEASE TELL US THE COMPANY NAMES (List all please)
                                WITH THE NAME THE CLIENTS KNEW YOU AS: examples: [format = Company\Your name]
                                which means enter your info like this: 'Psychic Contact 'Joe Light', 
                                TarotQuest 'Stargazer', Psychicfair 'Suzie', etc.
                            </label>
                            <div >                            
                                <textarea name='companies' placeholder='Type the company names here' style='height: 150px;' class='form-control'> <?=$information->companies?> </textarea>
                            </div>
                        </div>


                        <div class='form-group'>
                            <label for='firstname'>
                                <h2>Articles / Blogs </h2>
                            </label>                    
                        </div>


                        <div class='form-group'>
                            <label >
                                'PLEASE ENTER YOUR ARTICLE(s) or Furnish us with links on the web to your
                                blog(s) and/or actual articles written by you (This cannot be left blank)
                            </label>
                            <div>     
                                <label><input type='radio' name='articles' value='n' <?php if($information->articles == 'n' ){?>checked<?php }; ?>  > N/A</label>
                                <textarea name='articles' placeholder='List URLs here:' class='form-control' style='height: 150px;'><?=$information->articles?> </textarea>
                                <input type='file' style='margin-top: 5px' name='article_file' size='20' /> (File type: docx, doc, txt)

                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Blogs:
                            </label>
                            <div>     
                                <label><input type='radio' name='blogs' value='n' <?php if($information->blogs == 'n' ){?>checked<?php }; ?>  > N/A</label>
                                <textarea name='blogs' placeholder='List URLs of blogs you have contributed to here:' class='form-control' style='height: 150px;'> <?=$information->blogs?> </textarea>                                
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                Add anything else here that you think we may need to know about you
                            </label>
                            <div>
                                <label><input type='radio' name='bio' value='n' <?php if($information->bio == 'n' ){?>checked<?php }; ?> > N/A</label>
                                <textarea name='bio' placeholder='Type anything about you here:' class='form-control' style='height: 150px;'> <?=$information->bio?> </textarea>
                            </div>
                        </div>
  <div class='radio'>
  <label>
    <input type='radio' name='admin_priv' id='optionsRadios1' value='Reader' <?php if($information->optionsRadios2 == 'Reader' ){?>checked<?php }; ?>  >
    Reader
  </label>
</div>
<div class='radio'>
  <label>
    <input type='radio' name='admin_priv' id='optionsRadios2' value='Author' <?php if($information->optionsRadios2 == 'Author' ){?>checked<?php }; ?>  >
    Author
  </label>
</div>


   <div class='g-recaptcha' data-sitekey='6Ldh8BYTAAAAAJiD7_oqxFMLByX6unUoyRtxKZcz'></div>
    <div class='form-group'>
        <div class='col-md-3 col-md-offset-3'>
            <div id='messages'></div>
        </div>
    </div>
   
            <div style='text-align: center; width:100%; padding-top: 20px;'><button  name='button' id='button' type='submit'>Signup</button></div>

</form>

<script type="text/javascript">$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            exp_years: {
                validators: {
                    notEmpty: {
                        message: 'The experience years is required and cannot be empty'
                    }
                }
            },
            b_year: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_day: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_month: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: 'The phone number is required and cannot be empty'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>


    <?=$pages->content4?>
 <?php  /*include('formula/side_menu.php'); */?>

<?=$pages->content5?>


  <?php include('footer.php');?>



</body>
</html>