<meta name="robots" content="noindex">
    <div class='content'>
		
		<!-- Side Content -->
        
       
		
		<!-- Main Content -->
		<div class="main_content"> 
			
			<div>
				<?=utf8_encode(html_entity_decode($content))?>
			</div>

		</div>
        
        
		 <div class='side_content'>			
			<?=$this->load->view('registration/client_register_form2', null, TRUE)?>		
		</div>
	  
		<div class='clear'></div>
	
	</div>

    <script>

        var current = 1;
        var timeout = 5000;
        var imageArray = [];
        var totalImages = 0;

        $(function(){

            $('#slideshow img').each(function(i, object){
                imageArray.push(object);
            });

            //--- Set the animation
            totalImages = imageArray.length;
            setInterval(showSlideshowImage, timeout);
            showSlideshowImage();

        });

        function showSlideshowImage(){

            var objectImage = imageArray[current-1];
            var lastObjectImage = imageArray[(current == 1 ? totalImages-1 : current)];

            $(lastObjectImage).fadeOut('slow');
            $(objectImage).fadeIn('slow');

            current = (current == totalImages ? 1 : current+1);

        }

    </script>