<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$pages->title?></title>	
	<?php include('style.php');?>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
<style>
div.upload {
  text-align:center;
  
    width: 157px;
    height: 57px;
    background: url(https://lh6.googleusercontent.com/-dqTIJRTqEAQ/UJaofTQm3hI/AAAAAAAABHo/w7ruR1SOIsA/s157/upload.png);
    overflow: hidden;
}

div.upload input {
    display: block !important;
    width: 157px !important;
    height: 57px !important;
    opacity: 0 !important;
    overflow: hidden !important;
}

.fileinpt{
padding-left: 0%;
float: left;padding-top:15px;padding-bottom:15px;
overflow: hidden;
}
@media screen and (max-width:700px){
.fileinpt{
  float: left;padding-top:15px;padding-bottom:15px;
padding-left: 0%;
overflow: hidden;
}
@media screen and (max-width:530px){
.respon{
  transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
}
.fileinpt{
padding-left: 0%;
float: left;padding-top:15px;padding-bottom:15px;
}
@media screen and (max-width:350px){
.fileinpt{
  padding-left: 0px;
  text-align: left;
  float: left;padding-top:15px;padding-bottom:15px;
  overflow: hidden;
}
}
</style>
  

</head>
<body>
	<?php include('menu.php');?>
<div  id="slide">
<br /><br /><br /><br />
    
    <!-- MENU --> <div id="author">

      
      <div id="alg_profile"><div id="profile">
<form action="https://www.astral-foundations.com/users/profupdate/" method="post" enctype="multipart/form-data">
      <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Change Information</div>
      <p><a href="https://www.astral-foundations.com/users/pass/">Change Password</a></p>
        <div class="form-group" style="text-align:center; width:100%;">
        <div class="form-group" style="text-align:center; margin:auto; ">     
          <div class='form-group'>Please upload an image to appear with your profile/article(s) </div>
        <input id="uploadFile"  style="" class="form-control" placeholder="Choose File" disabled="disabled" />
        <div class="fileinpt">  
        <div class="upload"  style="text-align:center;">
          <input type="file"class="form-control" name="userfile" id="userfile" value="fichero"/>
         </div>
         </div>
        </div>
  </div>
        
      <!-- FORM -->
       <div class="form-group" style="text-align:center; width:100%;">
    <input type="text" class="form-control" id="username" name="username" placeholder="Email" value="<?=$profile->username?>">
 </div><div style="padding-top:10px;">
    <button  name="button" id="button" type="submit">Update</button>
    </div>  </form> <!-- FORM -->
  </div></div> <!-- INFO -->
        
        
      <!-- INFO --> </div>
      <script type="text/javascript">
document.getElementById("userfile").onchange = function () {
    document.getElementById("uploadFile").value = this.value;
};

$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            exp_years: {
                validators: {
                    notEmpty: {
                        message: 'The experience years is required and cannot be empty'
                    }
                }
            },
            b_year: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_day: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_month: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: 'The phone number is required and cannot be empty'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>
    </div></div> <!-- FOOTER -->
 <?php include('footer.php');?>
     

</body>
</html>