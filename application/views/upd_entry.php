<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <style>

body{background-image: url(https://www.astral-foundations.com/public/img/astral-foundations_featured-bg.jpg);
        background-repeat: repeat;
    background-position: center center;}

div.upload {
    text-align:center;
    
    width: 157px;
    height: 57px;
    background: url(https://lh6.googleusercontent.com/-dqTIJRTqEAQ/UJaofTQm3hI/AAAAAAAABHo/w7ruR1SOIsA/s157/upload.png);
    overflow: hidden;
}

div.upload input {
    display: block !important;
    width: 157px !important;
    height: 57px !important;
    opacity: 0 !important;
    overflow: hidden !important;
}

.fileinpt{
padding-left: 0%;
float: left;padding-top:15px;padding-bottom:15px;
overflow: hidden;
}
@media screen and (max-width:700px){
.fileinpt{
  float: left;padding-top:15px;padding-bottom:15px;
padding-left: 0%;
overflow: hidden;
}
@media screen and (max-width:530px){
.respon{
  transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
}
.fileinpt{
padding-left: 0%;
float: left;padding-top:15px;padding-bottom:15px;
}
@media screen and (max-width:350px){
.fileinpt{
  padding-left: 0px;
  text-align: left;
  float: left;padding-top:15px;padding-bottom:15px;
  overflow: hidden;
}
}
</style>
</head>
<body>
	<?php include('menu.php');?>

<br /><br /><br /><br />



        <!-- FORM--><div id="container">
<div class="container">


<?php 
      
        $hidden = array('id' => $entry_data->id);
    ?>
      <?=form_open_multipart(base_url().'users/update_entry/', '', $hidden)?>
       <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Please Update this Article</div>
        <?php if($entry_data->cover == ''){}else{?>
        <img src="<?=base_url().'public/articles/'.$entry_data->cover?>" alt="..." class="img-thumbnail" height="500">
<?php }; ?>
        <label for="title" class="sr-only">Title</label>
        <input type="text" id="title" name="title" value="<?=$entry_data->title?>" class="form-control" placeholder="Title" required autofocus> <br />
        Cover<div class="form-group" style="text-align:center; width:100%;">
                <div class="form-group" style="text-align:center; margin:auto; ">         
        <input id="uploadFile"  style="" class="form-control" placeholder="Choose File" disabled="disabled" />
        <div class="fileinpt" style="">  
        <div class="upload"  style="text-align:center;">
          <input type="file"class="form-control" name="userfile" id="userfile" value="fichero"/>
         </div>
         </div>
        </div>
  </div>
        <br /><br />
        <textarea class="ckeditor" id="content"name="content"><?=$entry_data->content?></textarea><br />
        <label for="tags" class="sr-only">Tags</label><br />
        <input type="text" id="tags"name="tags" class="form-control" placeholder="Tags" value="<?=$entry_data->tags?>" required>
         <br />
 <select  name="category"  class="form-control">

         <?php foreach($categories as $cat):?>
        <option value="<?=$cat->id?>"<?php if($entry_data->category == $cat->id){echo 'selected';}?> ><?=$cat->name_cat?></option>
        <?php endforeach;?>
        </select> 
        
           <br /><br />

                <div style="text-align: center; width:100%; padding-top: 20px;"><button  name="button" id="button" type="submit">Update</button></div>
      

    </div>
        </div>
<!-- FORM -->
     

<?php include('footer.php');?>

</body>
</html>
