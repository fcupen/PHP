
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script> 
  <script type="text/javascript" src="public/validator.js"></script>   
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

</head>
<body>
  <?php include('menu.php');?>
  <!-- MENU -->

<div id="slide">  
<div id="author">
<div id="link"><div id="text"><a href="https://www.astral-foundations.com/"><font style="color:white;"> Home</font></a> > <strong>Sign In</strong></div></div>
    
    <div id="alg_profile" >
  
    
    <div id="profile" >
  
<div>
<!-- FORM -->
      <form  id="contactForm" class="form-horizontal" action="https://www.astral-foundations.com/users/validate/" method="POST">
        <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Please sign in</div>
        <?php echo (isset($error)) ? '<p><font style="color:red;">This Username/Password was incorrect! Please Check the captcha!</font></p>' : '';?>
 <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="username" name="username"  placeholder="Email"/>
                </div>
            </div>
<div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-6">
                    <input  type="password" id="password"name="password" class="form-control" placeholder="Password"  />
                </div>
            </div>
<div class="form-group">
                <label class="col-md-3 control-label">Captcha</label>
                <div class="col-md-6">
                    <div class="g-recaptcha" data-sitekey="6Ldh8BYTAAAAAJiD7_oqxFMLByX6unUoyRtxKZcz"  style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>

                </div>
            </div>

        <div class="form-group">
        
        <div class="col-md-3 control-label">

          <label>
            <a href="https://www.astral-foundations.com/users/recover/">Recover Password</a>
         </label>
        </div> 
        </div>
         
        
        <div style="text-align: center; width:100%; padding-top: 20px;">
          <div >
         <div>
        
        </div>
        </div>
        
  <div class="form-group">
        <div class="col-md-3 col-md-offset-3">
            <div id="messages"></div>
        </div>
    </div>
   </div>
  <div class="form-group">
        <div class="col-md-3 col-md-offset-3">

        <div style="text-align: center; width:100%; padding-top: 20px;"><button  name="button" id="button" type="submit">Sign in</button></div>
        </div>
    </div>
      </form>
<!-- FORM -->

    </div>
<!-- CODE -->
<script type="text/javascript">$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>
<!-- CODE -->

    </div>
    
    
       


 
    </div>
    </div>
</div>


<!-- FOOTER -->
  <?php include('footer.php');?>



</body>
</html>

