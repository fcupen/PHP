<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Edit Article</title>	
	<?php include('style.php');?>
</head>
<body>
	<?php 
	    include('menu.php'); 
	    $hidden = array('id' => $entry_data->id);
	?>
	<?=form_open(base_url().'users/update_entry/', '', $hidden)?>
	<p>Title: <?=form_input('title', $entry_data->title)?></p>
	<p>Content: <?=form_textarea('content', $entry_data->content)?></p>
	<p>Tags: <?=form_input('tags', $entry_data->tags)?> (comma separated)</p>
	<?=form_submit('submit', 'Update')?>
	<?php include('footer.php');?>

</body>
</html>