<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$pages->title?></title>	
	<?php include('style.php');?>
  <style type="text/css">.borderless td, .borderless th {
    border: none;
}

.table-borderless tbody tr td, .table-borderless tbody tr th, .table-borderless thead tr th {
    border: none;
}</style>
</head>
<body>
	<?php include('menu.php');?>
	


<div id="slide">    
<div id="author">
<div id="link"><div id="text">
<!-- MENU -->
<a href="https://www.astral-foundations.com/"><font style="color:white;"> Home</font></a> > <strong>Article </strong>
    
    
     
     <!-- ARTICLE -->
</div></div>
<div id="alg_profile" >
  
    
    <div id="profile" >
<div id="titlf" > 
    <?=$entr->title?>
       </div>
  
    <img src="<?=base_url().'public/articles/'.$entr->cover?>" alt="..." class="img-thumbnail" width="100%" >
    
    <div id="text_profile">
        
 
<!-- ENTRY-->
 <div>
<div>
<br /><br />
 <?php 
        $aux_2='0';
        if (!$this->session->userdata('is_logged_in')){
            $query2 = $entr->content;
        $porc2 =explode(',', $query2);
        for ($i=0; $i < 4; $i++) { 
            if($porc2[$i] != ''){if ($i == '0') {
                $cont=$porc2[$i];
            }else{$cont.=', '.$porc2[$i];};}else{
                                                if ($aux_2=='0') {
                                                    $cont.='... Please <a href="'.base_url().'users/signin/">Sign In</a>';
                                                    $aux_2='1';
                                                }
                                                
                                            };
        }
//$cont+=$porc2['0'].', '.$porc2['1'].', '.$porc2['2'].', '.$porc2['3'].', '.$porc2['4'].'... Please <a href="'.base_url().'users/signin/">Sign In</a>';

            }else{$cont=$entr->content;};?>


        <?=$cont?>
        <p><strong> Author:</strong> <?=$entr->name?><br />

       <strong> Date: </strong><?=$entr->date?><br />
        <?php if($like->like == 'y'){ ?><a href="<?=base_url().'users/like_delete/'.$this->uri->segment(3)?>"><img src="<?=base_url().'public/img/like.png'?>"></a><?php }else{ ?><a href="<?=base_url().'users/like/'.$this->uri->segment(3)?>"><img src="<?=base_url().'public/img/dislike.png'?>" ></a><?php }; ?><br />
          <strong>Tags: </strong><?=$entr->tags?><hr /></p>

            </div>
</div>
  <!-- ENTRY-->

        
        
<!-- FORM-->
<table class="table table-borderless borderless" ><tr> <td>
    

    <?php if($this->session->userdata('is_logged_in')) : ?>
            Your comment: 


            <?=form_open(base_url().'users/comment/')?></td></tr><tr> <td>
            

            <?=form_hidden('id_article', $this->uri->segment(3))?></td></tr><tr> <td>
            
<label for="comment" class="sr-only">Comment</label>
        <input type="text" id="comment" name="comment" class="form-control" placeholder="Comment" required autofocus>
            </td></tr><tr> <td>
            
<button  type="submit" name="button" id="button" >Send</button>
            
            

            <?=form_close()?>
    <?php endif; ?>

</td></tr></table>
 <!-- FORM-->
 
<!-- COMMENT-->
 <table class="table table-borderless borderless"><tr><td>
    <?php
        if(!empty($comments)){
            echo '<h3>Comments</h3></td></tr><tr> <td>';
            foreach($comments as $comment)
                echo '<h4>'.$comment->author.'</h4></td></tr><tr> <td>'.
                $comment->comment.'</td></tr><tr> <td><br />'.
                $comment->date.'</td></tr><tr> <td><hr />';
        }
        else
            echo '<h3>No Comments!</h3>';
    ?>
    </td></tr></table>
 <!-- COMMENT-->

    </div>
    </div>
    
    
    
    <div id="side_bar">
<div style="background-color: #000;border-top-left-radius: 18px;border-top-right-radius: 18px;">
<div id="title" >RELATED LINKS</div></div>
<!-- SIDE BAR-->
 <?php  include('formula/side_menu.php'); ?>
 
<!-- SIDE BAR-->
</div>  


   
 
    </div>
    </div>
</div>
<!-- CENTRAL MENU-->

    
<div id="panel" > 
    <div id="btn_panel" >
    <div id="aln_panel">
<!-- CENTRAL MENU-->
<?php  include('formula/central_menu.php'); ?>
</div>
    </div>
</div>

<!-- CENTRAL MENU-->
    


	<?php include('footer.php');?>

</body>
</html>