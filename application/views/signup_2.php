
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?=$pages->title?></title> 
  <?php include('style.php');?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <?php include('style.php');?>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script> 
  <script type="text/javascript" src="public/validator.js"></script>   
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

</head>
<body>
  <?php include('menu.php');?>
  <!-- MENU -->

<div id="slide">  
<div id="author">
<div id="link"><div id="text"><a href="https://www.astral-foundations.com/"><font style="color:white;"> Home</font></a> > <strong>Sign Up</strong></div></div>
    
    <div id="alg_profile" >
  
    
    <div id="profile" >
  
<div>
<?php if(isset($error)){/*echo $error;*/}?>
<?php foreach ($success as $item => $value) {
  echo '<li>'.$item.' - '.$value.'</li>';
}?>

<form  id="contactForm"   action="<?=base_url().'users/register2/';?>" method="POST"> 
  <div style="font-size:29px;
    font-weight:bolder;
    padding-bottom:25px;">Sign Up</div>
   <h1>PLEASE ANSWER THE FOLLOWING AS TRUTHFULLY AS POSSIBLE</h1>
                          
                        <div class='form-group'>
                            <label>Are you a U.S. resident?: (If so, we will ask for it later) </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q1' id='q1' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q1' id='q1' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>SSN/SIN (If so, we will ask for it later)  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q2' id='q2' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q2' id='q2' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Will you be able to furnish us with your Govt. issued Photo ID (driver's license/passport/med I.D. card etc.)?
                        INSTRUCTIONS FOR SENDING YOUR PHOTO I.D.WILL BE EMAILED TO YOU UPON ACCEPTANCE </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q3' id='q3' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q3' id='q3' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Do you know any readers or clients on our site(s)? </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q4' id='q4' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q4' id='q4' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Are you related to, or know any other recent applicants to our site(s)?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q5' id='q5' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q5' id='q5' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Are you willing to be tested?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q6' id='q6' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q6' id='q6' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label for='username'>Are you willing to promote our sites by writing articles and/or blogging?
                        If you have no experience with this- are you willing to learn?  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q7' id='q7' value='n'>
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='q7' id='q7' value='y'>
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                List your experience in Article Writing and/or Blogging:
                            </label>
                            <div >                            
                                <input type='text' name='writing_years' style='width: 15%; display: inline' class='form-control' value='' /> years
                                <input type='radio' name='writing_years' value='n' > No &nbsp;                                
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                Do you have a Skype acct? Skype is required!!!
                                <br/>(It's how the Admin communicates with the Readers)
                            </label>
                            <div>                            
                                <input type='radio' name='has_skype' value='n' > No &nbsp;
                                <input type='radio' name='has_skype' value='y' > Yes
                                <br/>
                                <input type='text' placeholder='PLEASE ENTER YOUR SKYPE CONTACT ID HERE' name='skype_id' id='skype_id' class='form-control' value='' />
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >Do you have a PayPal acct?
                        (Required)  </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='has_paypal' id='has_paypal' value='n' >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='has_paypal' id='has_paypal' value='y' >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >If you are based in the USA: Do you have a U.S. based checking acct?
                        (you will need one so that we can pay you via Direct Deposit)</label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='based_usa' id='based_usa' value='n' >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='based_usa' id='based_usa' value='y' >
                                Yes
                              </label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >Are you currently on any govt. disability or subsidy program? </label>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='disability' id='disability' value='n' >
                                No
                              </label>
                            </div>
                            <div class='radio'>
                              <label>
                                <input type='radio' name='disability' id='disability' value='y' >
                                Yes
                              </label>
                            </div>
                        </div>

<h1>Your Computer / Internet / Technical Information</h1>

                        <div class='form-group'>
                            <label for='ISP'>Who is your ISP? (Internet Service Provider)</label>
                            <input type='text' class='form-control' id='ISP' name='ISP' placeholder='Who is your ISP?'>
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>How old is your Computer/Device</b>
                                <br/>(the one you will use for our sites/Readings)
                            </label>
                            <div>                            
                                <input type='text' name='pc_age' style='width:15%; display: inline;' class='form-control' value=''>/years
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>Operating System</b>
                            </label>
                            <div >          
                                <input type='checkbox' name='device[]' value='mac' > MAC:
                                <input type='text' name='device_mac' value='' class='form-control' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]' value='ipad' > I-Pad:
                                <input type='text' name='device_ipad' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]' value='ios' > iOS:
                                <input type='text' name='device_ios' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]' value='droid' > Droid:
                                <input type='text' name='device_droid' class='form-control' value='' style='width: 40%; display: inline' /> <br/><br/>
                                <input type='checkbox' name='device[]' value='other' > Other:
                                <input type='text' name='device_other' class='form-control' value='' style='width: 60%; display: inline' /> <br/><br/>                                
                            </div>
                        </div>

<br/><br/><h1>Your Qualifications</h1>

                          <div class='form-group'>
                            <label>
                                <b>Do you read and write English fluently?:</b>
                                (This website supports primarily the English language. 
                                BUT WE WILL CONSIDER ACCEPTING APPLICANTS THAT CAN ALSO GIVE CLIENTS READINGS IN OTHER LANGUAGES.
                                (Applicants who require translation software will not be considered.)
                            </label>
                            <div >                            
                                <label><input type='radio' name='en_fluent' value='y' > Yes, I am fluent in English </label><br/>
                                <label><input type='radio' name='en_fluent' value='n' > No, I am NOT fluent in English</label>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Other Languages:</b>
                            </label>
                            <div >                            
                                <input type='text' name='languages' class='form-control' value=''>
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                <b>Please rate your grammar and communication skills:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='com_skills' value='1' > 1 (Low)
                                <input type='radio' name='com_skills' value='2' > 2 (Ok)
                                <input type='radio' name='com_skills' value='3' > 3 (Good)
                                <input type='radio' name='com_skills' value='4' > 4 (Great)
                                <input type='radio' name='com_skills' value='5' > 5 (Excellent)
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Do you type at least 60 words per minute accurately?:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='typing_skills' value='n' > No
                                <input type='radio' name='typing_skills' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >
                                <b>Are you able to commit to a minimum of 15 hours per week?:</b>
                            </label>
                            <div >                            
                                <input type='radio' name='is_committed' value='n' > No
                                <input type='radio' name='is_committed' value='y' > Yes
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                Are you available to log on to take client readings/calls during Late Hours/“Wee” hours? (from midnight to 3am EST)
                                <br/>
                                NOTE: IT IS REQUIRED THAT YOU AGREE TO DOING “LATE SHIFT READINGS- AT LEAST ONE NIGHT PER WEEK).
                            </label>
                            <div >                            
                                <input type='radio' name='is_wee_available' value='n' > No
                                <input type='radio' name='is_wee_available' value='y' > Yes
                            </div>
                        </div>

                        <div class='form-group'>
                            <label >
                                If accepted: Date Available For Starting Work?:
                            </label>
                            <div>                            
                                <input type='text' name='start_date' id='start_date' class='form-control' style='width: 30%' value='' />                                
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                <b>What days of the week do you prefer working: </b>
                                <br/>(select ALL if you plan on working 7 days a week - consistently)
                            </label>
                            <div>                            
                                <input type='checkbox' name='date_avail[]' value='mon' > Mondays
                                <input type='checkbox' name='date_avail[]' value='tue' > Tuesdays
                                <input type='checkbox' name='date_avail[]' value='wed' > Wednesdays
                                <input type='checkbox' name='date_avail[]' value='thu' > Thursdays
                                <input type='checkbox' name='date_avail[]' value='fri' > Fridays
                                <input type='checkbox' name='date_avail[]' value='sat' > Saturdays
                                <input type='checkbox' name='date_avail[]' value='sun' > Sundays
                            </div>
                        </div>



                        <div class='form-group'>
                            <label >
                                <b>Areas of Expertise:</b>
                            </label>
                            <div >                            
                                <input type='checkbox' name='expertise[]' value='Love / Romance / Relationship Advice' > Love / Romance / Relationship Advice <br />
                                <input type='checkbox' name='expertise[]' value='Career / Finance / Money Advice' > Career / Finance / Money Advice <br />
                                <input type='checkbox' name='expertise[]' value='Tarot Cart Readings' > Tarot Cart Readings <br />
                                <input type='checkbox' name='expertise[]' value='Health' > Health (WE URGE CAUTION WITH THESE TYPES OF READINGS!) <br />
                                <input type='checkbox' name='expertise[]' value='Animal Communication' > Animal Communication <br />
                                <input type='checkbox' name='expertise[]' value='Any Holistic Therapies' > Any Holistic Therapies <br />
                                <input type='checkbox' name='expertise[]' value='Angels / Guides' > Angels / Guides <br />
                                <input type='checkbox' name='expertise[]' value='Channeling / Mediumship' > Channeling / Mediumship <br />
                                <input type='checkbox' name='expertise[]' value='Dream Interpretation' > Dream Interpretation <br />
                                <input type='checkbox' name='expertise[]' value='Past Life Analysis' > Past Life Analysis <br />
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                <b>What forms of <i>Divination Tools</i> do you use for your Readings?:</b> 
                                (You must choose at least one- Please list all that are applicable- NOTE: Some of these
                                may be used on your profile for our sites- if accepted)
                            </label>
                            <div>                            
                                <input type='checkbox' name='divine_practices[]' value='Numerology' > Numerology <br />
                                <input type='checkbox' name='divine_practices[]' value='Astrology / Horoscopes' > Astrology / Horoscopes <br />
                                <input type='checkbox' name='divine_practices[]' value='Palm Reading' > Palm Reading <br />
                                <input type='checkbox' name='divine_practices[]' value='Runes' > Runes <br />
                                <input type='checkbox' name='divine_practices[]' value='Cartomancy' > Cartomancy <br />
                                <input type='checkbox' name='divine_practices[]' value='Tarot' > Tarot <br />
                                <input type='checkbox' name='divine_practices[]' value='Pendulum' > Pendulum <br />
                                <input type='checkbox' name='divine_practices[]' value='Tea Leaves' > Tea Leaves <br />
                                <input type='checkbox' name='divine_practices[]' value='Crystals' > Crystals <br />
                                <input type='checkbox' name='divine_practices[]' value='Other' > Other 
                                <input type='text' name='divine_other' value='' class='form-control' style='width: 60%' value='' />
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                Do you consider yourself:
                                <br/>Clairvoyant?
                            </label>
                            <div>                            
                                <input type='radio' name='is_clairvoyant' value='n' > No
                                <input type='radio' name='is_clairvoyant' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >                        
                                Empathic?
                            </label>
                            <div >                            
                                <input type='radio' name='is_empathic' value='n' > No
                                <input type='radio' name='is_empathic' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label >                        
                                Clairsentient?
                            </label>
                            <div >                            
                                <input type='radio' name='is_clairsentient' value='n' > No
                                <input type='radio' name='is_clairsentient' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>                        
                                Other?
                            </label>
                            <div>                            
                                <input type='text' name='other_self' value='' class='form-control' />
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                What method of delivering Readings do you prefer to use (select yes to all that apply to you):
                                <br/>Email:
                            </label>
                            <div >                            
                                <input type='radio' name='method_email' value='n' > No
                                <input type='radio' name='method_email' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Chat:
                            </label>
                            <div >                            
                                <input type='radio' name='method_chat' value='n' > No
                                <input type='radio' name='method_chat' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Phone:
                            </label>
                            <div>                            
                                <input type='radio' name='method_phone' value='n' > No
                                <input type='radio' name='method_phone' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Video Chat:
                            </label>
                            <div>                            
                                <input type='radio' name='method_vidchat' value='n' > No
                                <input type='radio' name='method_vidchat' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                SMS Text:
                            </label>
                            <div>                            
                                <input type='radio' name='method_sms' value='n' > No
                                <input type='radio' name='method_sms' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Private In Person:
                            </label>
                            <div>                            
                                <input type='radio' name='method_private_person' value='n' > No
                                <input type='radio' name='method_private_person' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Radio:
                            </label>
                            <div >                            
                                <input type='radio' name='method_radio' value='n' > No
                                <input type='radio' name='method_radio' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                T.V:
                            </label>
                            <div>                            
                                <input type='radio' name='method_tv' value='n' > No
                                <input type='radio' name='method_tv' value='y' > Yes
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Public Venue:
                            </label>
                            <div>                            
                                <input type='radio' name='method_public_venue' value='n' > No
                                <input type='radio' name='method_public_venue' value='y' > Yes
                            </div>
                        </div>

                        <div class='form-group'>
                            <label>
                                <b>List All Your <i>NON</i> Divination Skills (in particular as related to the Internet):</b>
                            </label>
                            <div>
                                <label><input type='radio' name='skills' value='n' > N/A</label>
                                <textarea name='skills' placeholder='Type your NON Divination Skills here' class='form-control' style='height: 150px;'></textarea>
                            </div>
                        </div>
                        <br /><br /><h1>Your Experience</h1>

                        <div class='form-group'>
                            <label >
                                <b>*Years of Reading Experience:</b>
                            </label>
                            <div >                            
                                <input type='text' name='exp_years' class='form-control' style='width:15%; display: inline' value=''> years
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                <b>Companies Worked For:</b> PLEASE TELL US THE COMPANY NAMES (List all please)
                                WITH THE NAME THE CLIENTS KNEW YOU AS: examples: [format = Company\Your name]
                                which means enter your info like this: 'Psychic Contact 'Joe Light', 
                                TarotQuest 'Stargazer', Psychicfair 'Suzie', etc.
                            </label>
                            <div >                            
                                <textarea name='companies' placeholder='Type the company names here' style='height: 150px;' class='form-control'></textarea>
                            </div>
                        </div>


                        <div class='form-group'>
                            <label for='firstname'>
                                <h2>Articles / Blogs </h2>
                            </label>                    
                        </div>


                        <div class='form-group'>
                            <label >
                                'PLEASE ENTER YOUR ARTICLE(s) or Furnish us with links on the web to your
                                blog(s) and/or actual articles written by you (This cannot be left blank)
                            </label>
                            <div>     
                                <label><input type='radio' name='articles' value='N' > N/A</label>
                                <textarea name='articles' placeholder='List URLs here:' class='form-control' style='height: 150px;'></textarea>
                                
                            </div>
                        </div>
                        <div class='form-group'>
                            <label>
                                Blogs:
                            </label>
                            <div>     
                                <label><input type='radio' name='blogs' value='n' > N/A</label>
                                <textarea name='blogs' placeholder='List URLs of blogs you have contributed to here:' class='form-control' style='height: 150px;'></textarea>                                
                            </div>
                        </div>


                        <div class='form-group'>
                            <label >
                                Add anything else here that you think we may need to know about you
                            </label>
                            <div>
                                <label><input type='radio' name='bio' value='n' > N/A</label>
                                <textarea name='bio' placeholder='Type anything about you here:' class='form-control' style='height: 150px;'></textarea>
                            </div>
                        </div>
 
            <div style="text-align: center; width:100%; padding-top: 20px;"><button  name="button" id="button" type="submit">Signup</button></div>

</form>



<script type="text/javascript">$(document).ready(function() {
    $('#contactForm').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The Username is required and cannot be empty'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            exp_years: {
                validators: {
                    notEmpty: {
                        message: 'The experience years is required and cannot be empty'
                    }
                }
            },
            b_year: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_day: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            b_month: {
                validators: {
                    notEmpty: {
                        message: 'The Birthday is required and cannot be empty'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: 'The phone number is required and cannot be empty'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password address is required and cannot be empty'
                    }
                }
            }
        }
    });
});</script>


   </div>
    
    
    
    </div>
    

    </div>
    
    
    
    <!-- SIDE BAR-->
 <?php  /*include('formula/side_menu.php');*/ ?>

<!-- SIDE BAR-->
 
    </div>
    </div>
</div> <!-- FOOTER -->


  <?php include('footer.php');?>

</body>
</html>


