/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.autoParagraph = false;
	config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
	config.protectedSource.push( /<(html)?[\s\S]*?\/>/gi ) ;
	config.allowedContent = true;
	
	config.protectedSource.push( /<(br)?[\s\S]*?\/>/gi ) ;
	config.extraAllowedContent = 'dl dt dd html ?php ? a label form br';
	config.extraAllowedContent = 'div(*)';
	//config.allowedContent  = false;
	//config.allowedContent = 'h1 h2 h3 p html ?php ? body header head';
};
