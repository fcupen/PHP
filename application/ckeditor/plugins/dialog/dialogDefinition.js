﻿/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
*/
// Fix file input definition:
        CKEDITOR.ui.dialog.file.prototype.getValue = function()
        {
            return this.getInputElement().$.value;
        };